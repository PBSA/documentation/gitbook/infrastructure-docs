# What's Next?

## What's Next?

### 1. Auto-starting your node

Up until this point we have been running the node in the foreground which is fragile and inconvenient. So let's start the node as a service when the system boots up instead.

{% content-ref url="../the-basics/auto-starting-a-node.md" %}
[auto-starting-a-node.md](../the-basics/auto-starting-a-node.md)
{% endcontent-ref %}

### 2. Creating a backup node

After that, it would be smart to create a backup server to enable you to make software updates, troubleshoot issues with the node, and otherwise take your node offline without causing service outages.

{% content-ref url="../the-basics/backup-servers.md" %}
[backup-servers.md](../the-basics/backup-servers.md)
{% endcontent-ref %}

### 3. Fire up another node :fire:&#x20;

You've got a Witness node. Now you'll need a BOS node. And since you're in the node making mood, how about a SON too?

{% content-ref url="broken-reference" %}
[Broken link](broken-reference)
{% endcontent-ref %}

{% content-ref url="broken-reference" %}
[Broken link](broken-reference)
{% endcontent-ref %}

### 4. Enable SSL to encrypt your node traffic

If you have a node that is accessible from the internet (for example, an API or Seed node) it would be wise to enable SSL connections to your node.

{% content-ref url="../advanced-topics/reverse-proxy-for-enabling-ssl.md" %}
[reverse-proxy-for-enabling-ssl.md](../advanced-topics/reverse-proxy-for-enabling-ssl.md)
{% endcontent-ref %}

