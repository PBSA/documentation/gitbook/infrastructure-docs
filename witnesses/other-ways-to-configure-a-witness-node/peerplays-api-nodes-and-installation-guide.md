---
description: The list of Peerplays mainnet API nodes are given below
---

# Peerplays API nodes & Installation Guide

A list of public full API nodes for the Peerplays blockchain is maintained as a Github gist:

{% embed url="https://gist.github.com/pbsa-dev/024a306a5dc41fd56bd8656c96d73fd0" %}
Peerplays API nodes
{% endembed %}

## &#x20;API node Installation Guide

## 1. Introduction

The API node is a computer or virtual machine participating in the Peerplays Peer-to-Peer (P2P) network and maintaining an open port for queries and other client interactions. It provides a gateway to the blockchain by exposing a client API (Application Programming Interface) for inspecting or interacting with the blockchain. API nodes usually function as a back-end service to front-end software that provides a user interface (such as a graphical “wallet” interface), but they can also be queried directly with command-line tools for blockchain introspection.

**Node types:**

| P2P Node: | <ul><li>Relays blocks and transactions</li><li>Can be a “seed node” (helps other nodes sync blocks)</li></ul>                                                                                                 |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| API Node: | <ul><li>A P2P node, but also:</li><li>Exposes API for inspecting blockchain state and/or broadcasting transactions</li></ul>                                                                                  |
| BP Node:  | <ul><li>A P2P Node, but also:</li><li>Produces blocks for the network (BP = Block Producing)</li><li>Must be elected by stakeholders to produce blocks; is rewarded for successful block production</li></ul> |



### **Installation target**

The instructions below detail the installation and setup of a private API node that connects to the Peerplays public Test Net.  The install base is an Ubuntu 20.04 Virtual Machine with root access via SSH, such as one might acquire from a cloud compute provider like DigitalOcean or Linode.  The requirements for a test net API node are lightweight — 1 GB RAM and 25 GB disk should be sufficient.

{% hint style="info" %}
**Note:**  The steps here are for the public test net.  For a main net deployment, some steps will differ and the CPU, RAM, and disk requirements may be steeper.
{% endhint %}

## 2. Initial login and VM setup

After instantiating a VM or cloud compute machine based on Ubuntu 20.04 or similar, the first step is to connect to it in a terminal window using “ssh” and your node name. The below command and output is an example:

```nix
$ ssh peerplays-testnet-node
```

## 3. Installation of node software

The below commands will be executed in the newly deployed node terminal, which will act as a private API node.&#x20;

### 3.1 System updates

1. The number of software packages to be updated can be determined by using the command “apt update”. The output for the command is provided below:

**Example output:**

```nix
root@peerplays-testnet-04:~# apt update
```

{% code overflow="wrap" %}
```nix
Hit:1 https://repos-droplet.digitalocean.com/apt/droplet-agent main InRelease
Hit:2 http://mirrors.digitalocean.com/ubuntu focal InRelease
.
.
Translation-en [181 kB]
Fetched 5468 kB in 2s (3202 kB/s)
Reading package lists... Done
Building dependency tree
Reading state information... Done
68 packages can be upgraded. Run 'apt list --upgradable' to see them.
```
{% endcode %}

Similarly,  to see the list of packages run the “apt list --upgradable” command.

**Example output:**

```nix
root@peerplays-testnet-04:~# apt list --upgradable
```

{% code overflow="wrap" %}
```nix

Listing... Done
apparmor/focal-updates 2.13.3-7ubuntu5.2 amd64 [upgradable from: 2.13.3-7ubuntu5.1]
apport/focal-updates 2.20.11-0ubuntu27.27 all [upgradable from: 2.20.11-0ubuntu27.26]
base-files/focal-updates 11ubuntu5.7 amd64 [upgradable from: 11ubuntu5.5]
bolt/focal-updates 0.9.1-2~ubuntu20.04.2 amd64 [upgradable from: 0.9.1-2~ubuntu20.04.1]
bsdutils/focal-updates 1:2.34-0.1ubuntu9.4 amd64 [upgradable from: 1:2.34-0.1ubuntu9.3]
.
.
.
.
tcpdump/focal-updates 4.9.3-4ubuntu0.2 amd64 [upgradable from: 4.9.3-4ubuntu0.1]
tzdata/focal-updates 2023c-0ubuntu0.20.04.2 all [upgradable from: 2023c-0ubuntu0.20.04.0]
ubuntu-advantage-tools/focal-updates 28.1~20.04 amd64 [upgradable from: 27.9~20.04.1]
ubuntu-release-upgrader-core/focal-updates 1:20.04.41 all [upgradable from: 1:20.04.38]
udev/focal-updates 245.4-4ubuntu3.22 amd64 [upgradable from: 245.4-4ubuntu3.20]
ufw/focal-updates 0.36-6ubuntu1.1 all [upgradable from: 0.36-6ubuntu1]
update-manager-core/focal-updates 1:20.04.10.11 all [upgradable from: 1:20.04.10.10]
update-notifier-common/focal-updates 3.192.30.17 all [upgradable from: 3.192.30.11]
util-linux/focal-updates 2.34-0.1ubuntu9.4 amd64 [upgradable from: 2.34-0.1ubuntu9.3]
uuid-runtime/focal-updates 2.34-0.1ubuntu9.4 amd64 [upgradable from: 2.34-0.1ubuntu9.3]
```
{% endcode %}

### 3.2 Software Packages Upgrade

After collecting the list of software packages, run the command “apt upgrade” to begin the upgrade. The command will build, extract, unpack, and install the package.

**Example output:**

```nix
root@peerplays-testnet-04:~# apt upgrade
```

{% code overflow="wrap" %}
```nix

Reading package lists... Done
Building dependency tree
Reading state information... Done
Calculating upgrade... Done
The following NEW packages will be installed:
  libxmlb2
The following packages will be upgraded:
  apparmor apport base-files bolt bsdutils cloud-init distro-info distro-info-data fdisk fwupd fwupd-signed grub-common grub-efi-amd64-bin grub-efi-amd64-signed grub-pc grub-pc-bin grub2-common iptables
  isc-dhcp-client isc-dhcp-common libapparmor1 libblkid1 libfdisk1n amd64 base-files amd64 11ubuntu5.7 [60.4 kB]
Get:3 http://mirrors.digitalocean.com/ubuntu focal-updates/main amd64 bsdutils amd64 1:2.34-0.1ubuntu9.4 [63.0 kB].
.
.
Get:68 http://mirrors.digitalocean.com/ubuntu focal-updates/main amd64 sosreport amd64 4.4-1ubuntu0.20.04.1 [305 kB]
Get:69 http://mirrors.digitalocean.com/ubuntu focal-updates/main amd64
Preparing to unpack .../base-files_11ubuntu5.7_amd64.deb ...
Warning: Stopping motd-news.service, but it can still be activated by:
  motd-news.timer.
.
.
Unpacking software-properties-common (0.99.9.11) over (0.99.9.8) ...
Preparing to unpack .../41-python3-software-properties_0.99.9.11_all.deb ...
Unpacking python3-software-properties (0.99.9.11) over (0.99.9.8) ...
Preparing to unpack .../42-shim-signed_1.40.9+15.7-0ubuntu1_amd64.deb ...
Unpacking shim-signed (1.40.9+15.7-0ubuntu1) over (1.40.7+15.4-0ubuntu9) ...
Preparing to unpack .../43-sosreport_4.4-1ubuntu0.20.04.1_amd64.deb ...
Unpacking sosreport (4.4-1ubuntu0.20.04.1) over (4.3-1ubuntu0.20.04.2) ...
Preparing to unpack .../44-cloud-init_23.2.1-0ubuntu0~20.04.2_all.deb ...
Reloading AppArmor profiles
Skipping profile in /etc/apparmor.d/disable: usr.sbin.rsyslogd
Setting up python3-debian (0.1.36ubuntu1.1) ...
Setting up udev (245.4-4ubuntu3.22) ...
update-initramfs: deferring update (trigger activated)
Setting up libxtables12:amd64 (1.8.4-3ubuntu2.1) ...
Setting up sosreport (4.4-1ubuntu0.20.04.1) ...
Setting up libfdisk1:amd64 (2.34-0.1ubuntu9.4) ...
Setting up python-apt-common (2.0.1ubuntu0.20.04.1) ...


.
.
.
Sourcing file `/etc/default/grub'.
Setting up systemd-timesyncd (245.4-4ubuntu3.22) ...
Setting up ubuntu-release-upgrader-core (1:20.04.41) ...
Setting up python3-update-manager (1:20.04.10.11) ...
Setting up systemd-sysv (245.4-4ubu
.
.
.
Installing new version of config file /etc/cloud/templates/ntp.conf.sles.tmpl ...
update-initramfs: deferring update (trigger activated)
Processing triggers for dbus (1.12.16-2ubuntu2.3) ...
Processing triggers for install-info (6.7.0.dfsg.2-5) ...
Processing triggers for mime-support (3.64ubuntu1) ...
Processing triggers for initramfs-tools (0.136ubuntu6.7) ...
update-initramfs: Generating /boot/initrd.img-5.4.0-155-generic
root@peerplays-testnet-04:~#
```
{% endcode %}

### 3.3 Getting the software

The two executables needed are witness\_node and cli\_wallet.  These can be compiled, or built, via the instructions at [https://gitlab.com/PBSA/peerplays](https://gitlab.com/PBSA/peerplays).  Often, however, pre-built binaries for your system (e.g. Ubuntu 20.04 on AMD64 hardware) may be available in the “Releases” tab in the GitLab repo.  The instructions that follow involve retrieving pre-built binaries that were compiled for “TESTNET”.

The files related to witness\_node and cli\_wallet should be stored in an accessible directory and it is handy to keep subdirectories based on software versions, to make it easy to select different builds in the future. So, create a directory as follows:

```nix
root@peerplays-testnet-04: mkdir -p sw/peerplays/testnet-1.5.24-beta
root@peerplays-testnet-04: cd sw/peerplays/testnet-1.5.24-beta/


root@peerplays-testnet-04:~/sw/peerplays/testnet-1.5.24-beta# pwd
/root/sw/peerplays/testnet-1.5.24-beta
```

### 3.4 Retrieve the software:

In the directory created above, run the following commands to configure the witness node and cli wallet related files,

**Example Output:**

**To configure the witness node use the below command**

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/sw/peerplays/testnet-1.5.24-beta# wget https://gitlab.com/PBSA/peerplays/-/jobs/4624328583/artifacts/raw/build/programs/witness_node/witness_node
```
{% endcode %}

{% code overflow="wrap" %}
```nix
 
--2023-08-10 19:53:59--  https://gitlab.com/PBSA/peerplays/-/jobs/4624328583/artifacts/raw/build/programs/witness_node/witness_node
Resolving gitlab.com (gitlab.com)... 172.65.251.78, 2606:4700:90:0:f22e:fbec:5bed:a9b9
Connecting to gitlab.com (gitlab.com)|172.65.251.78|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 75554808 (72M) [application/octet-stream]
Saving to: ‘witness_node’
witness_node                                         100%[=====================================================================================================================>]  72.05M   106MB/s    in 0.7s
2023-08-10 19:54:00 (106 MB/s) - ‘witness_node’ saved [75554808/75554808]
```
{% endcode %}

**To configure the cli wallet use the below command:**

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/sw/peerplays/testnet-1.5.24-beta# wget https://gitlab.com/PBSA/peerplays/-/jobs/4624328583/artifacts/raw/build/programs/cli_wallet/cli_wallet
```
{% endcode %}

{% code overflow="wrap" %}
```nix
--2023-08-10 19:56:50--  https://gitlab.com/PBSA/peerplays/-/jobs/4624328583/artifacts/raw/build/programs/cli_wallet/cli_wallet
Resolving gitlab.com (gitlab.com)... 172.65.251.78, 2606:4700:90:0:f22e:fbec:5bed:a9b9
Connecting to gitlab.com (gitlab.com)|172.65.251.78|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 85884416 (82M) [application/octet-stream]
Saving to: ‘cli_wallet’
cli_wallet                                           100%[=====================================================================================================================>]  81.91M  95.9MB/s    in 0.9s

2023-08-10 19:56:51 (95.9 MB/s) - ‘cli_wallet’ saved [85884416/85884416]

root@peerplays-testnet-04:~/sw/peerplays/testnet-1.5.24-beta# pwd
/root/sw/peerplays/testnet-1.5.24-beta
```
{% endcode %}

Now the witness node and cli wallet file will be created in the desired folder.

### 3.5 File Permissions

To make the witness node and cli wallet files executable, the file permission must be given accordingly. Use the below command to provide necessary permission. The “a+x” makes the file executable for all users.

**Example Output:**

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/sw/peerplays/testnet-1.5.24-beta# chmod a+x witness_node cli_wallet 
```
{% endcode %}

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/sw/peerplays/testnet-1.5.24-beta# ls -l
total 157664
-rwxr-xr-x 1 root root 75554808 Aug 10 19:54 witness_node
-rwxr-xr-x 1 root root 85884416 Aug 10 19:56 cli_wallet
```
{% endcode %}

The necessary file permissions are given to the files.

### 3.6 Library Installation

#### 1.  LibZMQ and LibBoost

&#x20;     Boost is an open source software development library which provides various tools and utilities for C++ programmers. The C++ framework relies on the boost libraries. The boost is mainly used to create high quality, efficient, and portable c++ code.

**Output:**

```nix
root@peerplays-testnet-04:~# apt install libzmq5
```

{% code overflow="wrap" %}
```nix
Reading package lists... Done
Building dependency tree
Reading state information... Done
libzmq5 is already the newest version (4.3.2-2ubuntu1).
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
root@peerplays-testnet-04:~# apt install libboost1.71-all-dev
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following additional packages will be installed:
  autoconf automake autotools-dev binutils binutils-common binutils-x86-64-linux-gnu cpp cpp-8 cpp-9 gcc gcc-7-base gcc-8 gcc-8-base gcc-9 gcc-9-base gfortran gfortran-8 gfortran-9 ibverbs-providers
  icu-devtools libasan4 libasan5 libatomic1 libbinutils 
.
.
Setting up libboost-timer1.71-dev:amd64 (1.71.0-6ubuntu6) ...
Setting up python3.8-dev (3.8.10-0ubuntu1~20.04.8) ...
Setting up libboost-type-erasure1.71-dev:amd64 (1.71.0-6ubuntu6) ...
Setting up libboost-mpi-python1.71-dev (1.71.0-6ubuntu6) ...
Setting up libboost1.71-all-dev (1.71.0-6ubuntu6) ...
Processing triggers for install-info (6.7.0.dfsg.2-5) ...
Processing triggers for libc-bin (2.31-0ubuntu9.9) ...
Processing triggers for man-db (2.9.1-1) ...
root@peerplays-testnet-04:~#
```
{% endcode %}

#### 2. Other runtime libraries: `libBitcoin`

A runtime library is a collection of executable software programs used at program run time to provide one or more native program functions or services. Peerplays witness\_node software depends on some libraries that are not widely available. We have built these libraries and hosted them for convenient download.

#### **2.1 Getting software package**

**Output:**

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~# wget https://peerplays.download/downloads/debian-files/aio-libraries/peerplays-runtime-libs.deb
```
{% endcode %}

{% code overflow="wrap" %}
```nix
--2023-08-10 20:20:50--  https://peerplays.download/downloads/debian-files/aio-libraries/peerplays-runtime-libs.deb
Resolving peerplays.download (peerplays.download)... 96.46.48.223
Connecting to peerplays.download (peerplays.download)|96.46.48.223|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 105585324 (101M) [application/octet-stream]
Saving to: ‘peerplays-runtime-libs.deb’


peerplays-runtime-libs.deb                           100%[=====================================================================================================================>] 100.69M  9.90MB/s    in 10s

2023-08-10 20:21:00 (9.73 MB/s) - ‘peerplays-runtime-libs.deb’ saved [105585324/105585324]

root@peerplays-testnet-04:~#
```
{% endcode %}



**2.2 Installation of the runtime libraries**

The following command will install the run-time lib. If you observe the permission denied in the output, it can be ignored as the installation will be done directly through the root in some cases.&#x20;

**Output:**

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~# apt install -f ./peerplays-runtime-libs.deb
```
{% endcode %}

{% code overflow="wrap" %}
```nix
Reading package lists... Done
Building dependency tree
Reading state information... Done
Note, selecting 'peerplays-runtime-libs' instead of './peerplays-runtime-libs.deb'
The following NEW packages will be installed:
  peerplays-runtime-libs
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 0 B/106 MB of archives.
After this operation, 0 B of additional disk space will be used.
Get:1 /root/peerplays-runtime-libs.deb peerplays-runtime-libs amd64 1.0.0 [106 MB]
Selecting previously unselected package peerplays-runtime-libs.
(Reading database ... 119263 files and directories currently installed.)
Preparing to unpack .../peerplays-runtime-libs.deb ...
Unpacking peerplays-runtime-libs (1.0.0) ...
Setting up peerplays-runtime-libs (1.0.0) ...
Processing triggers for libc-bin (2.31-0ubuntu9.9) ...
N: Download is performed unsandboxed as root as file '/root/peerplays-runtime-libs.deb' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
root@peerplays-testnet-04:~#
```
{% endcode %}



**2.3 Checking the library file installation**

To make sure the library files are installed correctly, run the following command in the terminal,

**Output:**

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/sw/peerplays/testnet-1.5.24-beta# ls /usr/local/lib/peerplays-runtime/
```
{% endcode %}

{% code overflow="wrap" %}
```nix
libbitcoin-blockchain.so        libbitcoin-network.so         libboost_atomic.so            libboost_filesystem.so.1.72.0  libboost_log_setup.so.1.72           libboost_system.so.1
libbitcoin-blockchain.so.0      libbitcoin-network.so.0       libboost_atomic.so.1          libboost_iostreams.so          libboost_log_setup.so.1.72.0         libboost_system.so.1.72
libbitcoin-blockchain.so.0.0.0  libbitcoin-network.so.0.0.0   libboost_atomic.so.1.72       libboost_iostreams.so.1        libboost_prg_exec_monitor.so         libboost_system.so.1.72.0
libbitcoin-client.so            libbitcoin-node.so            libboost_atomic.so.1.72.0     libboost_iostreams.so.1.72     libboost_prg_exec_monitor.so.1       libboost_thread.so
libbitcoin-client.so.0          libbitcoin-node.so.0          libboost_chrono.so            libboost_iostreams.so.1.72.0   
.
.
libbitcoin-explorer.so.0        libbitcoin-system.so.0        libboost_filesystem.so.1      libboost_log_setup.so          libboost_regex.so.1.72.0             libsecp256k1.so.0.0.0
libbitcoin-explorer.so.0.0.0    libbitcoin-system.so.0.0.0    libboost_filesystem.so.1.72   libboost_log_setup.so.1 libboost_system.so
```
{% endcode %}

### 3.7 Witness node version check

&#x20;Run the below command to check the witness node version and other build versions.

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/sw/peerplays/testnet-1.5.24-beta# pwd
/root/sw/peerplays/testnet-1.5.24-beta
```
{% endcode %}

**Output:**

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/sw/peerplays/testnet-1.5.24-beta# ./witness_node --version
```
{% endcode %}

<pre class="language-nix"><code class="lang-nix"><strong>Version: 1.5.24-beta
</strong>Git Revision: accd334a86cf4c1d1aeedd22c4384e2506b81718
Built: Jul 10 2023 at 11:42:39
SSL: OpenSSL 1.1.1f  31 Mar 2020
Boost: 1.72
root@peerplays-testnet-04:~/sw/peerplays/testnet-1.5.24-beta#
</code></pre>

### 3.8 Move to the home directory

Run the below command,

```nix
root@peerplays-testnet-04:~/sw/peerplays/testnet-1.5.24-beta# cd ~
root@peerplays-testnet-04:~# pwd
/root
root@peerplays-testnet-04:~#
```

### 3.9 Create necessary Directories

Next we will create the folders in which the witness node will store block data and configuration files.

#### 1. peerplays-testnet directory

Run the below command to create the required directory and move to the location: **/root/Node/peerplays-testnet**

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~# mkdir -p Node/peerplays-testnet
root@peerplays-testnet-04:~# ls
Node  peerplays-runtime-libs.deb  snap  sw
root@peerplays-testnet-04:~#
root@peerplays-testnet-04:~# cd Node/peerplays-testnet/
root@peerplays-testnet-04:~/Node/peerplays-testnet# ls
root@peerplays-testnet-04:~/Node/peerplays-testnet# pwd
/root/Node/peerplays-testnet
```
{% endcode %}

#### 2. “bin” folder creation

The user should be in the location: /root/Node/peerplays-testnet, then create the bin folder. Below is the output,

```nix
root@peerplays-testnet-04:~/Node/peerplays-testnet# mkdir bin
root@peerplays-testnet-04:~/Node/peerplays-testnet# cd bin
root@peerplays-testnet-04:~/Node/peerplays-testnet/bin# pwd
/root/Node/peerplays-testnet/bi
```

#### 3. Symbolic link creation&#x20;

The ln command supports the symbolic link creation subcommand. It will build links or aliases to other files on our system. The ln -s commands create soft symbolic links.  We will use this to link to the current version of the software.  (This will make it easier to upgrade later.)

**Execute the following commands in terminal:**

The user should be in the directory “ **/root/Node/peerplays-testnet/bin** ”

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/Node/peerplays-testnet/bin# pwd
/root/Node/peerplays-testnet/bin
root@peerplays-testnet-04:~/Node/peerplays-testnet/bin#
root@peerplays-testnet-04:~/Node/peerplays-testnet/bin# ln -s /root/sw/peerplays/testnet-1.5.24-beta/ selected
root@peerplays-testnet-04:~/Node/peerplays-testnet/bin# ln -s selected/witness_node ./
root@peerplays-testnet-04:~/Node/peerplays-testnet/bin# ln -s selected/cli_wallet ./
```
{% endcode %}

## 4. Witness node update

### 4.1 Switch to “peerplays-testnet” directory

Execute the following commands in the terminal,

```nix
root@peerplays-testnet-04:~/Node/peerplays-testnet/bin# cd ..
root@peerplays-testnet-04:~/Node/peerplays-testnet# ls
bin
root@peerplays-testnet-04:~/Node/peerplays-testnet# pwd
/root/Node/peerplays-testnet/
```

### 4.2 Downloading the “Genesis” JSON file

The “genesis” file is a JSON description of the initial starting state of the Peerplays Public Test Net.  It is needed to allow the software to connect to and interoperate with the public test net.

Use the wget command to download the required file. Execute the following:

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/Node/peerplays-testnet# wget https://testnet.peerplays.download/genesis/public-testnet.json
--2023-08-11 19:22:51--  https://testnet.peerplays.download/genesis/public-testnet.json
Resolving testnet.peerplays.download (testnet.peerplays.download)... 96.46.48.69
Connecting to testnet.peerplays.download (testnet.peerplays.download)|96.46.48.69|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 21144 (21K) [application/json]
Saving to: ‘public-testnet.json’


public-testnet.json                                  100%[=====================================================================================================================>]  20.65K  --.-KB/s    in 0s


2023-08-11 19:22:51 (207 MB/s) - ‘public-testnet.json’ saved [21144/21144]


root@peerplays-testnet-04:~/Node/peerplays-testnet#
```
{% endcode %}

### 4.3 Compare the checksum value

After the Json file download, the file’s sha256 checksum value must be same as the given value here “195d4e865e3a27d2b204de759341e4738f778dd5c4e21860c7e8bf1bd9c79203”.&#x20;

Make sure both the values are the same.

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/Node/peerplays-testnet# sha256sum public-testnet.json
195d4e865e3a27d2b204de759341e4738f778dd5c4e21860c7e8bf1bd9c79203  public-testnet.json
root@peerplays-testnet-04:~/Node/peerplays-testnet#
```
{% endcode %}

### 4.4 Create Witness node directory

Running witness\_node for the first time will create the data directory structure for blocks and configuration. Execute the below command to create the witness node directory.  The software will hang after a while because it does not yet know how to connect to the correct network.  When console output stops, stop the witness\_node with Ctrl-C.

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/Node/peerplays-testnet# ./bin/witness_node
2209762ms th_a       config_util.cpp:230           create_new_config_fi ] Writing new config file at /root/Node/peerplays-testnet/witness_node_data_dir/config.ini
2209766ms th_a       config_util.cpp:292           create_logging_confi ] Writing new config file at /root/Node/peerplays-testnet/witness_node_data_dir/logging.ini
2209777ms th_a       accounts_list_plugin.cpp:128  list_accounts        ] accounts list plugin:  list_accounts()
2209777ms th_a       bookie_plugin.cpp:412         plugin_initialize    ] bookie plugin: plugin_startup() begin
.
.
.
               ] Started Peerplays node on a chain with 0 blocks.
2209972ms th_a       main.cpp:174                  main                 ] Chain ID is b3f7fe1e5ad0d2deca40a626a4404524f78e65c3a48137551c33ea4e7c365672


Ctrl-C
3290233ms asio       main.cpp:164                  operator()           ] Caught SIGINT attempting to exit cleanly
3290233ms th_a       main.cpp:177                  main                 ] Exiting from signal 2
3290284ms th_a       main.cpp:181                  main                 ] Witness node is closed and turned off
root@peerplays-testnet-04:~/Node/peerplays-testnet# ls
bin  public-testnet.json  witness_node_data_dir
```
{% endcode %}

Note that a new folder “witness\_node\_data\_dir” now exists.

### 4.5 Install the editor of your choice (VIM is used here)

Once the editor is installed, some values in the config file witness\_node\_data\_dir/config.ini need to be updated. But, before updating the file, you may wish to make a copy of the original config file with another name.

#### 1. Make a copy of original config.ini&#x20;

The below commands helps in terminal execution:

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/Node/peerplays-testnet#
root@peerplays-testnet-04:~/Node/peerplays-testnet# cd witness_node_data_dir/


root@peerplays-testnet-04:~/Node/peerplays-testnet/witness_node_data_dir# ls
blockchain  config.ini  logging.ini  logs  p2p
root@peerplays-testnet-04:~/Node/peerplays-testnet/witness_node_data_dir# cp config.ini config.ini-original
root@peerplays-testnet-04:~/Node/peerplays-testnet/witness_node_data_dir# ls
blockchain  config.ini  config.ini-original  logging.ini  logs  p2p
root@peerplays-testnet-04:~/Node/peerplays-testnet/witness_node_data_dir#
```
{% endcode %}

Now, the original config file is copied and saved as “config.ini-original”. The user can update the values in the config.ini file.

#### 2. Input values in config.ini file

The user must be in the directory, “/Node/peerplays-testnet# cd witness\_node\_data\_dir/” and open the config.ini file using the editor installed.\


The config.ini file is large and only the necessary values are mentioned here.

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/Node/peerplays-testnet# cd witness_node_data_dir/
root@peerplays-testnet-04:~/Node/peerplays-testnet/witness_node_data_dir# ls
blockchain  config.ini  config.ini-original  logging.ini  logs  p2p
```
{% endcode %}

{% code overflow="wrap" %}
```ini
root@peerplays-testnet-04:~/Node/peerplays-testnet/witness_node_data_dir# vi config.ini


# Endpoint for P2P node to listen on
p2p-endpoint = 0.0.0.0:19777


# P2P nodes to connect to on startup (may specify multiple times)
seed-node = 134.209.113.44:19777
seed-node = 24.199.104.149:19777
seed-node = 92.129.238.17:19777


# JSON array of P2P nodes to connect to on startup
seed-nodes = []


# Pairs of [BLOCK_NUM,BLOCK_ID] that should be enforced as checkpoints.
# checkpoint =


# Endpoint for websocket RPC to listen on
rpc-endpoint = 0.0.0.0:18090


# Endpoint for TLS websocket RPC to listen on
# rpc-tls-endpoint =


# The TLS certificate file for this server
# server-pem =


# Password for this certificate
# server-pem-password =


# File to read Genesis State from
genesis-json = "public-testnet.json"
```
{% endcode %}



Only the highlighted values in the above config.ini file must be updated based on the values of your node and save the file.

**Notes on listening ports:**



<table data-full-width="false"><thead><tr><th></th><th></th></tr></thead><tbody><tr><td>p2p-endpoint:</td><td><p>This is the port the node uses for P2P connections (for relaying blocks and transactions across the network).<br><br>127.0.0.1:19777 — Node will make out-going P2P connections to peers, but will not accept incoming connections from peers.</p><p><br></p><p>0.0.0.0:19777 — Node will accept incoming P2P connections in addition to making outgoing connections.  </p></td></tr><tr><td>rpc-endpoint:</td><td>This is the “client port” that client software (GUI interfaces, CLI wallets, etc.) connect to to interact with the network.<br><br>127.0.0.1:18090 — Connections allowed from “localhost” only — such as cli_wallet or a GUI wallet running on the same machine as the node.<br><br>0.0.0.0:18090 — Connections allowed from any machine.  Useful when the client software and the node are on separate machines, or when offering the API connection as a public service.</td></tr></tbody></table>

\


### 4.6 Initial synchronization of the node with the blockchain

After the config file is updated, the node must be resynced with the blockchain to reflect the changes. The user must be in the location “/root/Node/peerplays-testnet”.  This command should be executed only once to sync the chain. (If started with --resync-blockchain again in the future, the blocks database will be discarded and the entire blockchain will be downloaded again.) Execute the below command to perform the resync,

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/Node/peerplays-testnet# ./bin/witness_node --resync-blockchain
```
{% endcode %}

{% code overflow="wrap" %}
```nix
1432917ms th_a       accounts_list_plugin.cpp:128  list_accounts        ] accounts list plugin:  list_accounts()
1432918ms th_a       bookie_plugin.cpp:412         plugin_initialize    
.
.
         ] total_balances[asset_id_type()].value: 201002000000000 core_asset_data.current_supply.value: 201702000000000
1432954ms th_a       application.cpp:137           reset_p2p_node       ] Adding seed node 134.209.113.44:19777
1432954ms th_a       application.cpp:137           reset_p2p_node       ] Adding seed node 24.199.104.149:19777
1432954ms th_a       application.cpp:137           reset_p2p_node       ] Adding seed node 92.129.238.17:19777
```
{% endcode %}

After the sync is complete, the terminal will begin to show blocks received in real time.   You can stop the node at this point (with Ctrl-C), and in the future start it without the resync flag.

To start the node normally, run the below command:

```nix
root@peerplays-testnet-04:~/Node/peerplays-testnet# ./bin/witness_node 
```

## 5. Persisting the run environment with GNU Screen

The purpose of the “screen” command is to create a pseudo virtual terminal. Mainly used to keep the witness node running even after the terminal dies. The terminal must be screened before starting the execution, to keep the witness node alive at all times. Also, the node can be reattached from another terminal at any time.

Execute the below command to screen the terminal before starting the node:

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/Node/peerplays-testnet# screen -DRRS PeerplaysTestNet

//Start the witness node
root@peerplays-testnet-04:~/Node/peerplays-testnet# ./bin/witness_node

```
{% endcode %}

**To Detach the screen session**

Switch to the screen session, Press Ctrl+A and then D to detach from the screen.  It can later be reattached with the same “**screen -DRRS PeerplaysTestNet**” command

\
**To check the SCREEN session status**

When logging into the node, one can check for running screen sessions with:

{% code overflow="wrap" %}
```nix
root@peerplays-testnet-04:~/Node/peerplays-testnet# screen -ls
There is a screen on:
    135708.PeerplaysTestNet    (08/14/23 13:23:27)    (Detached)
1 Socket in /run/screen/S-root.

root@peerplays-testnet-04:~/Node/peerplays-testnet#
```
{% endcode %}

{% hint style="info" %}
The screen command used here is one of the options for persisting the terminal. Users can pick their own option to perform this operation.  Other options include tmux, pm2, Docker, and others.
{% endhint %}
