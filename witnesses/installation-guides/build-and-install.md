---
description: >-
  Manual installation steps to configure a witness node running on ubuntu
  18.04/20.04
---

# Build and Install

This is an introduction for a new witness node to get up to speed on the Peerplays blockchain. It is intended for Witnesses planning to join a live, already deployed, blockchain. The node running on ubuntu 18.04 or ubuntu 20.04 can follow the steps in the document for manual installation.

The following steps outline the manual installation of a Witness Node:

1. Preparing the Environment
2. Build Peerplays
3. Update the `config.ini` File
4. Start the node

## 1. Preparing the Environment

### 1.1. Hardware requirements

Please see the general Witness [hardware requirements](https://infra.peerplays.tech/the-basics/hardware-requirements).

For the manual install, the requirements that we'll need for this guide would be as follows (as per the hardware requirements doc):

| Node Type? | CPU     | Memory          | Storage   | Bandwidth | OS           |
| ---------- | ------- | --------------- | --------- | --------- | ------------ |
| Witness    | 4 Cores | 16GB :warning:  | 100GB SSD | 1Gbps     | Ubuntu 18.04 |

{% hint style="danger" %}
The memory requirements shown in the table above are adequate to operate the node. Building and installing the node from source code (as with this manual installation guide) will require more memory. You may run into errors during the build and install process if the system memory is too low. See [Installing vs Operating](../../the-basics/hardware-requirements.md#4-2-installing-vs-operating) for more details.
{% endhint %}

### 1.2. Installing the required dependencies

The following dependencies are necessary for a clean install of Ubuntu 20.04:

{% code overflow="wrap" %}
```
sudo apt-get install \
    autoconf bash bison build-essential ca-certificates dnsutils expect flex git \
    graphviz libbz2-dev libcurl4-openssl-dev libncurses-dev libpcre3-dev \
    libsnappy-dev libsodium-dev libssl-dev libtool libzip-dev locales lsb-release \
    mc nano net-tools ntp openssh-server pkg-config python3 python3-jinja2 sudo \
    systemd-coredump wget libboost-all-dev
```
{% endcode %}

### 1.3 cmake setup

Cmake is an open-source, cross platform tool that uses independent configuration files to generate native build tool files specific to the compiler and platform. It consists of precompiled binaries and the cmake tools makes configuration, building, and debugging much easier.

Install the cmake using the below commands:

{% code overflow="wrap" %}
```
wget https://github.com/Kitware/CMake/releases/download/v3.24.2/cmake-3.24.2-linux-x86_64.sh
chmod 755 ./cmake-3.24.2-linux-x86_64.sh
sudo ./cmake-3.24.2-linux-x86_64.sh --prefix=/usr --skip-license
cmake --version
```
{% endcode %}

### 1.4 Install Boost libraries

Boost libraries provide free peer-reviewed portable C++ source libraries and it can be used across broad spectrum of application.

Install Boost libraries using the below commands:

{% code overflow="wrap" %}
```
wget https://boostorg.jfrog.io/artifactory/main/release/1.72.0/source/boost_1_72_0.tar.gz
tar -xzvf boost_1_72_0.tar.gz
cd boost_1_72_0
./bootstrap.sh
./b2
sudo ./b2 install
sudo ldconfig
```
{% endcode %}

### 1.5 Install libzmq&#x20;

The components libzmp and cppzmp are used for relaying messages between nodes.&#x20;

First, install libzmq using the below commands:

{% code overflow="wrap" %}
```


wget https://github.com/zeromq/libzmq/archive/refs/tags/v4.3.4.tar.gz
tar -xzvf v4.3.4.tar.gz
cd libzmq-4.3.4
mkdir build
cd build
cmake ..
make -j$(nproc)
sudo make install
sudo ldconfig
```
{% endcode %}

### 1.6 Install cppzmq

Next, install cppzmp using the below commands:

```
wget https://github.com/zeromq/cppzmq/archive/refs/tags/v4.9.0.tar.gz
tar -xzvf v4.9.0.tar.gz
cd cppzmq-4.9.0
mkdir build
cd build
cmake ..
make -j$(nproc)
sudo make install
sudo ldconfig

```

### 1.7 Install gsl

GSL is the GNU scientific library for numerical computing. It is a collection of routines for numerical computing in e.g. linear algebra, probability, random number generation, statistics, differentiation, etc.,

Install the gsl using the below commands:

```
wget https://github.com/imatix/gsl/archive/refs/tags/v4.1.4.tar.gz
tar -xzvf v4.1.4.tar.gz
cd gsl-4.1.4
make -j$(nproc)
sudo make install
sudo ldconfig
```

### 1.8 Install libbitcoin

The libbitcoin toolkit is a set of cross platform C++ libraries for building bitcoin applications. The toolkit consists of several libraries, most of which depend on the base libbitcoin-system library.

Install the libbitcoin using the below commands:

{% code overflow="wrap" %}
```
git clone --branch version3.8.0 --depth 1 https://gitlab.com/PBSA/peerplays-1.0/libbitcoin-explorer.git
cd libbitcoin-explorer
sudo ./install.sh
sudo ldconfig
```
{% endcode %}

### 1.9 Install Doxygen

Doxygen is a software utility that recognizes comments within C++ code that have a certain form, and uses them to produce a collection of HTML files containing the information in those comments.

Install the Doxygen using the below commands:

```
wget https://github.com/doxygen/doxygen/archive/refs/tags/Release_1_8_17.tar.gz
tar -xvf Release_1_8_17.tar.gz
cd doxygen-Release_1_8_17
mkdir build
cd build
cmake ..
make -j$(nproc)
sudo make install
sudo ldconfig
```

### 2.0 Install Perl

Perl is a high-level, general-purpose, interpreted, dynamic programming language originally developed for text manipulation.&#x20;

Install the Perl using the below commands:

```
wget https://github.com/Perl/perl5/archive/refs/tags/v5.30.0.tar.gz
tar -xvf v5.30.0.tar.gz
cd perl5-5.30.0
./Configure -des
make -j$(nproc)
sudo make install
sudo ldconfig
```

## 2. Build Peerplays

Use the below commands to build Peerplays:

```
cd $HOME
git clone https://github.com/peerplays-network/peerplays
cd peerplays
git checkout 1.6.1 # Replace with most recent tag
git submodule update --init --recursive

# If you want to build Mainnet node
cmake -DCMAKE_BUILD_TYPE=Release

# If you want to build Testnet node
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_PEERPLAYS_TESTNET=1

# the following command will install the executable files under /usr/local
sudo make install

# the following isn't required if you ran the "sudo make install" command above.
# If you prefer, the "make -j$(nproc)" command will install the
# programs under $HOME/peerplays/programs
# Update the -j flag depending on your current system specs:
# Recommended 4GB of RAM per 1 CPU core
# make -j2 for 8GB RAM
# make -j4 for 16GB RAM
# make -j8 for 32GB RAM
make -j$(nproc)
```

### 2.1. Starting the Peerplays Witness Node

If we have installed the blockchain following the above steps, the node can be started as follows:

```
witness_node
```

Launching the Witness for the first time creates the directories which contain the configuration files.

{% hint style="warning" %}
Next, stop the Witness node before continuing (`Ctrl + c`).
{% endhint %}

## 3. Update the config.ini File

We need to set the endpoint and seed-node addresses so we can access the cli\_wallet and download all the initial blocks from the chain. Within the config.ini file, locate the p2p-endpoint, rpc-endpoint, and seed-node settings and enter the following addresses.

```
nano $HOME/witness_node_data_dir/config.ini

p2p-endpoint = 0.0.0.0:9777
rpc-endpoint = 127.0.0.1:8090
seed-node = ca.peerplays.info:9777
seed-node = de.peerplays.xyz:9777
seed-node = pl.peerplays.org:9777
seed-nodes = []
```

Save the changes and start the node back up.

```
witness_node
```

We have successfully started the witness node and it is now ready for configuration.

{% hint style="success" %}
Success! You built a Peerplays witness node from the latest source code and now it's up and running. :tada:&#x20;
{% endhint %}



## 4. Node Configuration

Next step is to configure the witness node based on the requirement. There are different ways in which the nodes can be configured such as block producer, SON node, API node, and delayed node.

1. Becoming a block producer is one of the important steps as it is mandatory to use the node for transactions across the wallet. Follow the steps from the below document to become a block producer,

{% embed url="https://peerplays.gitbook.io/peerplays-infrastructure-docs/~/changes/0IXXeM79c2vnSTcZBjuH/witnesses/how-to-become-a-block-producing-witness" %}

2. There are other ways it which the node can be configured. The below document showcase the other ways available for node configuration.

{% embed url="https://peerplays.gitbook.io/peerplays-infrastructure-docs/~/changes/0IXXeM79c2vnSTcZBjuH/witnesses/other-ways-to-configure-a-witness-node" %}

## 5. What's Next?

After configuring the node with desired configuration, click below to learn the NEXT steps :thumbsup:&#x20;

{% embed url="https://peerplays.gitbook.io/peerplays-infrastructure-docs/~/changes/0IXXeM79c2vnSTcZBjuH/witnesses/whats-next" %}

## 6. Glossary

**Witness:** An independent server operator which validates network transactions.

**Witness Node:** Nodes with a closed RPC port. They don't allow external connections. Instead these nodes focus on processing transactions into blocks.
