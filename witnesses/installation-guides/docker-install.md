---
description: Setup a Witness Node using a pre-configured Docker container
---

# Docker Install

This document assumes that you are running Ubuntu 18.04 or 20.04. Other Debian based releases may also work with the provided script.

The following steps outline the Docker installation of a Witness Node:

1. Preparing the Environment
2. Installing Docker
3. Installing the Peerplays image
4. Starting the Container
5. Update the `config.ini` File
6. Create a Peerplays Account
7. Update `config.ini` with Witness Account Info
8. Start the Container and Vote for Yourself

{% hint style="info" %}
Before we begin, to set up a Witness node requires about 15 PPY. This is to pay for an upgraded account (5 PPY) and to create a new witness (8 PPY). The remaining funds are to pay for various transaction fees while setting up the node (like voting for yourself!). Please see [Obtaining Your First Tokens](../../the-basics/obtaining-your-first-tokens.md) for more info.

Note that these fees will likely change over time as recommended by the Committee of Advisors.
{% endhint %}

## 1. Preparing the Environment

### 1.1 Hardware requirements

Please see the Witness node [hardware requirements](../../the-basics/hardware-requirements.md).

For the docker install on Peerplays **Mainnet**, the requirements that we'll need for this guide would be as follows (as per the hardware requirements doc):

| Node Type? | CPU     | Memory | Storage   | Bandwidth | OS           |
| ---------- | ------- | ------ | --------- | --------- | ------------ |
| Witness    | 4 Cores | 16GB   | 100GB SSD | 1Gbps     | Ubuntu 18.04 |

### 1.2 Installing the required dependencies

```
sudo apt-get update
sudo apt-get install git curl
```

Then we'll clone the Peerplays Docker repository.

```
git clone -b release https://gitlab.com/PBSA/tools-libs/peerplays-docker.git
```

## 2. Installing Docker

{% hint style="info" %}
It is required to have Docker installed on the system that will be performing the steps in this document.
{% endhint %}

Docker can be installed using the `run.sh` script inside the Peerplays Docker repository:

```
cd peerplays-docker
sudo ./run.sh install_docker
```

The `run.sh` script contains many commands to make managing the node easy. A list of all its [commands](https://infra.peerplays.tech/witnesses/installation-guides/docker-install#10.-docker-run.sh-commands-list) are listed in section 9 of this document.

Since the script has added the currently logged in user to the Docker group, you'll need to re-login (or close and reconnect SSH) for Docker to function correctly.

{% hint style="info" %}
You can look at [https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/) to learn more on how to install Docker. Or if you are having permission issues trying to run Docker, use `sudo` or look at [https://docs.docker.com/engine/install/linux-postinstall/](https://docs.docker.com/engine/install/linux-postinstall/).
{% endhint %}

### 2.1. Setting up the .env file

Copy the `example.env` to `.env` located in the root of the repository (ie peerplays-docker folder)&#x20;

```
cp example.env .env
```

We're going to have to make some changes to the `.env` file so we'll open that now using a text editor.

```
nano .env
```

Here are the important parts of the `.env` file. These will be the parts that need to be edited or optionally edited. The rest of the file should be unchanged.

```
# Comma separated port numbers to expose to the internet (binds to 0.0.0.0)
# Expose 9777 to the internet, but only expose RPC ports 8090 and 8091 onto 127.0.0.1 (localhost)
# allowing the host machine access to the container's RPC ports via 127.0.0.1:8090 and 127.0.0.1:8091
# We'll need ports 8090 and 8091 open to our localhost to interact with the Peerplays CLI Wallet.
PORTS=9777,127.0.0.1:8090:8090,127.0.0.1:8091:8091

# Websocket RPC node to use by default for ./run.sh remote_wallet
REMOTE_WS="wss://ca.peerplays.info/"
```

## 3. Installing the Peerplays image

Use `run.sh` to pull the node image:

```
cd ~/peerplays-docker
sudo ./run.sh install
```

## 4. Start the Container

With at least 8GB of disk space available in your home folder, we'll start the node. This will create and/or start the Peerplays docker container.

```
sudo ./run.sh replay
```

Then we'll check the status of the container to see if all is well.

```
sudo ./run.sh logs
```

Last we'll stop the container so we can make updates to the `config.ini` file.

```
sudo ./run.sh stop
```

## 5. Update the config.ini File

We need to set the endpoint and seed-node addresses so we can access the cli\_wallet and download all the initial blocks from the chain. Within the `config.ini` file, locate the p2p-endpoint, rpc-endpoint, and seed-node settings and enter the following addresses.

```
nano ~/peerplays-docker/data/witness_node_data_dir/config.ini

p2p-endpoint = 0.0.0.0:9777
rpc-endpoint = 127.0.0.1:8090
seed-node = ca.peerplays.info:9777
seed-node = de.peerplays.xyz:9777
seed-node = pl.peerplays.org:9777
seed-nodes = []
```

Save the changes and start the container back up.

```
sudo ./run.sh replay
```

We have successfully started the witness node and it is now ready for configuration.

## 7. Edit config.ini to include your Witness ID and your private key pair

Exit the cli\_wallet with the `quit` command. We'll stop the container and edit the `config.ini` file once again.

```
sudo ./run.sh stop
nano ~/peerplays-docker/data/witness_node_data_dir/config.ini

witness-id = "your_witness_id"
private-key = ["block_signing_key", "private_key_for_your_block_signing_key"]
```

## 8. Start the container and vote for yourself

```
sudo ./run.sh start
```

Once again, we need to wait for the node to sync the blocks to use the cli\_wallet. After the sync, you can vote for yourself.

```
sudo ./run.sh wallet
unlock your-password-here
vote_for_witness created-username created-username true true
```

Now you can check your votes to verify it worked.

```
get_witness your_witness_account
```

{% hint style="success" %}
Complete! You've installed your Witness node and you're up and running. :confetti\_ball:&#x20;
{% endhint %}

## 9. Node Configuration

Next step is to configure the witness node based on the requirement. There are different ways in which the nodes can be configured such as block producer, SON node, API node, and delayed node.

1. Becoming a block producer is one of the important steps as it is mandatory to use the node for transactions across the wallet. Follow the steps from the below document to become a block producer,

{% embed url="https://peerplays.gitbook.io/peerplays-infrastructure-docs/~/changes/0IXXeM79c2vnSTcZBjuH/witnesses/how-to-become-a-block-producing-witness" %}

2. There are other ways it which the node can be configured. The below document showcase the other ways available for node configuration.

{% embed url="https://peerplays.gitbook.io/peerplays-infrastructure-docs/~/changes/0IXXeM79c2vnSTcZBjuH/witnesses/other-ways-to-configure-a-witness-node" %}

## 10. What's Next?

After configuring the node with desired configuration, click below to learn the NEXT steps :thumbsup:&#x20;

{% embed url="https://peerplays.gitbook.io/peerplays-infrastructure-docs/~/changes/0IXXeM79c2vnSTcZBjuH/witnesses/whats-next" %}

## 11. Docker `run.sh` commands list

* `start` - starts seed container
* `start_son` - starts son seed container
* `start_son_regtest` - starts son seed container and bitcoind container under the docker network
* `clean` - Remove blockchain, p2p, and/or shared mem folder contents, seed, bitcoind, and son docker network (warns beforehand)
* `dlblocks` - download and decompress the blockchain to speed up your first start
* `replay` - starts seed container (in replay mode)
* `replay_son` - starts son seed container (in replay mode)
* `memory_replay` - starts seed container (in replay mode, with --memory-replay)
* `shm_size` - resizes /dev/shm to size given, e.g. ./run.sh shm\_size 10G
* `stop` - stops seed container
* `status` - show status of seed container
* `restart` - restarts seed container
* `install_docker` - install docker
* `install` - pulls latest docker image from server (no compiling)
* `install_full` - pulls latest (FULL NODE FOR RPC) docker image from server (no compiling)
* `rebuild` - builds seed container (from docker file), and then restarts it
* `build` - only builds seed container (from docker file)
* `logs` - show all logs inc. docker logs, and seed logs
* `wallet` - open cli\_wallet in the container
* `remote_wallet` - open cli\_wallet in the container connecting to a remote seed
* `enter` - enter a bash session in the currently running container
* `shell` - launch the seed container with appropriate mounts, then open bash for inspection

## 10. Glossary

**Witness:** An independent server operator which validates network transactions.

**Witness node:** Nodes with a closed RPC port. They don't allow external connections. Instead these nodes focus on processing transactions into blocks.
