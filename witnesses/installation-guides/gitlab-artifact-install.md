---
description: Setup a Witness Node using a pre-compiled GitLab artifact
---

# GitLab Artifact Install

This document assumes that you are running **Ubuntu 20.04**.

{% hint style="warning" %}
The Gitlab artifacts were built targeting Ubuntu 20.04 and will not work on Ubuntu 18.04. While Peerplays does support Ubuntu 18.04, you'll need to follow the [Manual Install guide](build-and-install.md) for Ubuntu  or use [Docker](docker-install.md) to use it on this version.
{% endhint %}

The following steps outline the artifact installation of a Witness Node:

1. Prepare the environment
2. Download and extract the artifacts
3. Copy the artifacts to the proper locations
4. Update the `config.ini` File
5. Start the node

## 1. Prepare the environment

### 1.1. Hardware requirements

Please see the general Witness [hardware requirements](https://infra.peerplays.tech/the-basics/hardware-requirements).

For the GitLab artifact install, the requirements that we'll need for this guide would be as follows:

| Node Type? | CPU     | Memory | Storage   | Bandwidth | OS           |
| ---------- | ------- | ------ | --------- | --------- | ------------ |
| Witness    | 4 Cores | 16GB   | 100GB SSD | 1Gbps     | Ubuntu 20.04 |

{% hint style="warning" %}
The artifacts from GitLab are already built for x86\_64 architecture. These will not work with ARM based architecture.
{% endhint %}

### 1.2. Install the required dependencies

The following dependencies are necessary for a clean install of Ubuntu 20.04:

```
sudo apt-get install \
    autoconf bash bison build-essential ca-certificates dnsutils expect flex git \
    graphviz libbz2-dev libcurl4-openssl-dev libncurses-dev libpcre3-dev \
    libsnappy-dev libsodium-dev libssl-dev libtool libzip-dev locales lsb-release \
    mc nano net-tools ntp openssh-server pkg-config python3 python3-jinja2 sudo \
    systemd-coredump wget libboost-all-dev
```

### 1.3 cmake setup

Cmake is an open-source, cross platform tool that uses independent configuration files to generate native build tool files specific to the compiler and platform. It consists of precompiled binaries and the cmake tools makes configuration, building, and debugging much easier.

Install the cmake using the below commands:

{% code overflow="wrap" %}
```
wget https://github.com/Kitware/CMake/releases/download/v3.24.2/cmake-3.24.2-linux-x86_64.sh
chmod 755 ./cmake-3.24.2-linux-x86_64.sh
sudo ./cmake-3.24.2-linux-x86_64.sh --prefix=/usr --skip-license
cmake --version
```
{% endcode %}

### 1.4 Install Boost libraries

Boost libraries provide free peer-reviewed portable C++ source libraries and it can be used across broad spectrum of application.

Install Boost libraries using the below commands:

{% code overflow="wrap" %}
```
wget https://boostorg.jfrog.io/artifactory/main/release/1.72.0/source/boost_1_72_0.tar.gz
tar -xzvf boost_1_72_0.tar.gz
cd boost_1_72_0
./bootstrap.sh
./b2
sudo ./b2 install
sudo ldconfig
```
{% endcode %}

### 1.5 Install libzmq&#x20;

The components libzmp and cppzmp are used for relaying messages between nodes.&#x20;

First, install libzmq using the below commands:

{% code overflow="wrap" %}
```


wget https://github.com/zeromq/libzmq/archive/refs/tags/v4.3.4.tar.gz
tar -xzvf v4.3.4.tar.gz
cd libzmq-4.3.4
mkdir build
cd build
cmake ..
make -j$(nproc)
sudo make install
sudo ldconfig
```
{% endcode %}

### 1.6 Install cppzmq

Next, install cppzmp using the below commands:

```
wget https://github.com/zeromq/cppzmq/archive/refs/tags/v4.9.0.tar.gz
tar -xzvf v4.9.0.tar.gz
cd cppzmq-4.9.0
mkdir build
cd build
cmake ..
make -j$(nproc)
sudo make install
sudo ldconfig

```

### 1.7 Install gsl

GSL is the GNU scientific library for numerical computing. It is a collection of routines for numerical computing in e.g. linear algebra, probability, random number generation, statistics, differentiation, etc.,

Install the gsl using the below commands:

```
wget https://github.com/imatix/gsl/archive/refs/tags/v4.1.4.tar.gz
tar -xzvf v4.1.4.tar.gz
cd gsl-4.1.4
make -j$(nproc)
sudo make install
sudo ldconfig
```

### 1.8 Install libbitcoin

The libbitcoin toolkit is a set of cross platform C++ libraries for building bitcoin applications. The toolkit consists of several libraries, most of which depend on the base libbitcoin-system library.

Install the libbitcoin using the below commands:

{% code overflow="wrap" %}
```
git clone --branch version3.8.0 --depth 1 https://gitlab.com/PBSA/peerplays-1.0/libbitcoin-explorer.git
cd libbitcoin-explorer
sudo ./install.sh
sudo ldconfig
```
{% endcode %}

### 1.9 Install Doxygen

Doxygen is a software utility that recognizes comments within C++ code that have a certain form, and uses them to produce a collection of HTML files containing the information in those comments.

Install the Doxygen using the below commands:

```
wget https://github.com/doxygen/doxygen/archive/refs/tags/Release_1_8_17.tar.gz
tar -xvf Release_1_8_17.tar.gz
cd doxygen-Release_1_8_17
mkdir build
cd build
cmake ..
make -j$(nproc)
sudo make install
sudo ldconfig
```

### 2.0 Install Perl

Perl is a high-level, general-purpose, interpreted, dynamic programming language originally developed for text manipulation.&#x20;

Install the Perl using the below commands:

```
wget https://github.com/Perl/perl5/archive/refs/tags/v5.30.0.tar.gz
tar -xvf v5.30.0.tar.gz
cd perl5-5.30.0
./Configure -des
make -j$(nproc)
sudo make install
sudo ldconfig
```

## 2. Download and extract the artifacts

Artifacts are pre-built binaries that are available to download from GitLab. You can see the available pipelines, sorted by release tags, on the GitLab [Peerplays project](https://gitlab.com/PBSA/peerplays/-/pipelines?scope=tags\&page=1) page. The link in the code below refers to release version `1.6.1` which is the latest production release as of the writing of this document. Please make sure to replace the tag with the one you need.

{% code overflow="wrap" %}
```
cd $HOME
mkdir artifacts
cd artifacts
sudo wget https://gitlab.com/PBSA/peerplays/-/jobs/artifacts/1.6.1/download?job=build-mainnet -O artifacts.zip
unzip artifacts.zip
```
{% endcode %}

{% hint style="warning" %}
Double check the tag in the download link!
{% endhint %}

## 3. Copy the artifacts to the proper locations

Putting the `witness_node` and `cli_wallet` programs in the `/usr/local/bin` directory will allow us to call the program from any directory.

{% code overflow="wrap" %}
```
sudo cp $HOME/artifacts/build/programs/witness_node/witness_node /usr/local/bin/witness_node
sudo cp $HOME/artifacts/build/programs/cli_wallet/cli_wallet /usr/local/bin/cli_wallet
```
{% endcode %}

Now we can run start the node with:

```
cd $HOME
witness_node
```

Launching the witness creates the required directories which contain the config.ini file we'll need to edit. We'll stop the witness now with `Ctrl + C` so we can edit the config file.

## 4. Edit the config.ini file

We need to set the endpoint and seed-node addresses so we can access the cli\_wallet and download all the initial blocks from the chain. Within the config.ini file, locate the p2p-endpoint, rpc-endpoint, and seed-node settings and enter the following addresses.

```
nano ~/witness_node_data_dir/config.ini

p2p-endpoint = 0.0.0.0:9777
rpc-endpoint = 0.0.0.0:8090
seed-node = ca.peerplays.info:9777
seed-node = de.peerplays.xyz:9777
seed-node = pl.peerplays.org:9777
seed-nodes = []
```

Save the changes and start the Witness back up.

```
witness_node
```

We have successfully started the witness node and it is now ready for configuration.

{% hint style="success" %}
Congrats! You've successfully installed your Witness node using GitLab artifacts! :confetti\_ball:&#x20;
{% endhint %}

## 5. Node Configuration

Next step is to configure the witness node based on the requirement. There are different ways in which the nodes can be configured such as block producer, SON node, API node, and delayed node.

1. Becoming a block producer is one of the important steps as it is mandatory to use the node for transactions across the wallet. Follow the steps from the below document to become a block producer,

{% embed url="https://peerplays.gitbook.io/peerplays-infrastructure-docs/~/changes/0IXXeM79c2vnSTcZBjuH/witnesses/how-to-become-a-block-producing-witness" %}

2. There are other ways it which the node can be configured. The below document showcase the other ways available for node configuration.

{% embed url="https://peerplays.gitbook.io/peerplays-infrastructure-docs/~/changes/0IXXeM79c2vnSTcZBjuH/witnesses/other-ways-to-configure-a-witness-node" %}

## 6. What's Next?

After configuring the node with desired configuration, click below to learn the NEXT steps :thumbsup:&#x20;

{% embed url="https://peerplays.gitbook.io/peerplays-infrastructure-docs/~/changes/0IXXeM79c2vnSTcZBjuH/witnesses/whats-next" %}

## 7. Glossary

**Witness:** An independent server operator which validates network transactions.

**Witness Node:** Nodes with a closed RPC port. They don't allow external connections. Instead these nodes focus on processing transactions into blocks.

