# Installation Guides

Based on the requirement, witness node can be installed using any of the below procedure.

{% content-ref url="build-and-install.md" %}
[build-and-install.md](build-and-install.md)
{% endcontent-ref %}

{% content-ref url="docker-install.md" %}
[docker-install.md](docker-install.md)
{% endcontent-ref %}

{% content-ref url="gitlab-artifact-install.md" %}
[gitlab-artifact-install.md](gitlab-artifact-install.md)
{% endcontent-ref %}

