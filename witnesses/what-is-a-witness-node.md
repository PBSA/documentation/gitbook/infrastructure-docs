# What is a Witness node?

In Peerplays, the word "witness" refers to a lot of terms and definitions. A "witness node" is typically a machine on the network that runs the Peerplays core software, and "witnesses" (observes) blocks, validates that they are correct, and relays them to the rest of the network. A witness node may also be responsible for producing blocks, by assembling transactions into a block structure and signing the block with an approved signing key. These special witness nodes are called "block-producing witness nodes" or "block producers".

To determine which witness nodes are allowed to produce blocks, holders of PPY can vote for "witnesses," which are accounts on the chain that have applied for a block-producing witness role. The top-voted witness accounts get to designate the signing key that will allow their witness nodes (the machines running Peerplays core software) to fulfill the block-producer role.

The Peerplays block-producing witnesses will bundle transactions into blocks and sign them with their signing keys.  Witnesses keep the blockchain alive by producing one block every three seconds.&#x20;

{% hint style="info" %}
For example, if there are 20 Witnesses, each would produce one block every minute on average, in a random rotation.
{% endhint %}

Witness nodes that are not block producers can serve a variety of other roles, including being an API node, a seed node, a SON node, or other purposes.

There are various types of procedures like Manual, Docker, and GitLab Artifact installation for the Witness node. Click the below link to learn about the installation steps involved in detail.

{% embed url="https://peerplays.gitbook.io/peerplays-infrastructure-docs/~/changes/0IXXeM79c2vnSTcZBjuH/witnesses/installation-guides" %}

The below page helps to learn about the Peerplays witness.

{% embed url="https://community.peerplays.com/witnesses/what-is-a-peerplays-witness" %}

