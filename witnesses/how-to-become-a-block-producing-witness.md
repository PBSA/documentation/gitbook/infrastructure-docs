# How to become a block producing witness?

The following document provides an overview of how to become a witness node, the perks of becoming a witness, duties of a witness, and a brief description about the node types.

{% embed url="https://community.peerplays.com/witnesses/becoming-a-witness" %}

## Prerequisites to become a block producing witness:

1. An existing account &#x20;
2. A machine running a witness\_node that can be configured to produce blocks

## Steps to become a block producing witness node

1. [Create a Peerplays account](how-to-become-a-block-producing-witness.md#id-1.-create-a-peerplays-account)&#x20;
2. [Upgrade account to Lifetime membership ](how-to-become-a-block-producing-witness.md#id-2.-upgrade-account-to-lifetime-membership)
3. [Configuring witness node as a Block producer](how-to-become-a-block-producing-witness.md#id-3.-configuring-witness-node-as-a-block-producer)
4. [Adding witness id and private key pair into config.ini file](how-to-become-a-block-producing-witness.md#id-4.-adding-witness-id-and-private-key-pair-into-config.ini-file)
5. [Start the witness node and vote for yourself](how-to-become-a-block-producing-witness.md#id-5.-start-the-witness-and-vote-for-yourself)

## 1. Create a Peerplays account

### Steps to register a new Peerplays account

The first peerplays account can be created in a flash using **Peerplays DEX** by following the steps in below document,

{% embed url="https://community.peerplays.com/technology/peerplays-nex/user-guide/account-creation" %}

Peerplays provides two networks on which  the user may create an account - main net and test net. To learn and familiarize the operations of a node use the test net account. After learning the process and to work on the real Peerplays network, use the main-net account. The [User Guide](https://community.peerplays.com/technology/peerplays-dex-1/user-guide) will help you navigate Peerplays DEX and learn about its features and options in detail.

Click the URL below to create an account and then to login Peerplays DEX,

1. **Mainnet Peerplays DEX access**

Click the below link to use the Main-net DEX:

{% embed url="https://swap.peerplays.com" %}

2. **Testnet Peerplays DEX access**

Click the below link to use the Test-net DEX:

{% embed url="https://testnet.peerplays.download/" %}

Account creation with Peerplays DEX

Click the below link to learn in detail about the Peerplays account creation&#x20;

{% embed url="https://peerplays.gitbook.io/peerplays-infrastructure-docs/~/changes/0IXXeM79c2vnSTcZBjuH/witnesses/how-to-create-a-peerplays-account" %}

## &#x20;2. Upgrade account to lifetime membership

The account should have some PPY balance to become a lifetime member. To create a witness, the account must have a lifetime membership status.

**Syntax:**

```
upgrade_account created-username true
```

**Example:**

```
upgrade_account testuser-01 true
```

**Output:**

<figure><img src="../.gitbook/assets/upgrade-true.JPG" alt=""><figcaption></figcaption></figure>

### 3. Configuring your account to serve as a block-producing Witness

Your account can be configured as an eligible "block producer" by using cli wallet. The commands to complete the configuration are explained in the below link.

### Create yourself as a witness

The next step is to create a witness

The URL in this command is your own URL which should point to a page which describes who you are and why you want to become a Peerplays witness. Note your block signing key after you enter this command.

{% hint style="warning" %}
To execute the command, some PPY is required.
{% endhint %}

**Syntax:**

```
create_witness created-username "https://your-url-to-witness-proposal" true
```

**Example:**

```
create_witness testuser-01 "test-witness" true
```

**Expected Output:**

<figure><img src="../.gitbook/assets/sign-in-key (1).JPG" alt=""><figcaption></figcaption></figure>

### Collecting the witness account info

The Block-signing key and private keys are required to get the witness id. From the above output the block-signing key is obtained and using that the private key can be generated.

**Syntax:**

```
get_private_key YOURBLOCKSIGNINGKEY
```

Then dump your keys to check and compare. One of the returned values from the following command should match your block\_signing\_key.

```
dump_private_keys
```

Last we'll get your witness ID.

```
get_witness created-username
```

**Example:**

```
get_private_key TEST5V---MWfn
dump_private_keys
get_witness testuser-01
```

**Expected Output:**

<figure><img src="../.gitbook/assets/witness-id.JPG" alt=""><figcaption></figcaption></figure>

## 4. Adding witness id and private key pair into config.ini file

Exit the cli\_wallet with the `quit` command. Back in the first command line window, we'll stop the node (`Ctrl + c`) and edit the `config.ini` file once again.

```
nano $HOME/witness_node_data_dir/config.ini

witness-id = "your_witness_id"
private-key = ["block_signing_key", "private_key_for_your_block_signing_key"]
```

## 5. Start the Witness and vote for yourself

```
witness_node
```

Once again, we need to wait for the node to sync the blocks to use the cli\_wallet. After the sync, you can vote for yourself. Back in the second command line window:

```
cli_wallet
unlock your-password-here
```

**Syntax:**

```
vote_for_witness created-username created-username true true
```

**Example Output:**

<figure><img src="../.gitbook/assets/voting-true.JPG" alt=""><figcaption></figcaption></figure>

Now you can check your votes to verify it worked.

**Syntax:**

```
get_witness your_witness_account 
```

**Example output:**

<figure><img src="../.gitbook/assets/witness-voting-number.JPG" alt=""><figcaption></figcaption></figure>
