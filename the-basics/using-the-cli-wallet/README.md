---
description: A guide for node operators
---

# Using the CLI Wallet

## 1. Table of Contents

### 1.1. CLI Wallet Fundamentals

{% content-ref url="cli-wallet-fundamentals.md" %}
[cli-wallet-fundamentals.md](cli-wallet-fundamentals.md)
{% endcontent-ref %}

This document explains installing the CLI Wallet, setting it up during its first use, and the basics of running the wallet.

### 1.2. CLI Commands for All Nodes

{% content-ref url="cli-commands-for-all-nodes.md" %}
[cli-commands-for-all-nodes.md](cli-commands-for-all-nodes.md)
{% endcontent-ref %}

This reference doc contains the following commands:

* suggest\_brain\_key
* get\_private\_key\_from\_password
* import\_key
* upgrade\_account
* create\_vesting\_balance
* get\_private\_key
* dump\_private\_keys
* get\_account

### 1.3. CLI Commands for Witnesses

{% content-ref url="cli-commands-for-witnesses.md" %}
[cli-commands-for-witnesses.md](cli-commands-for-witnesses.md)
{% endcontent-ref %}

This reference doc contains the following commands:

* create\_witness
* update\_witness
* get\_witness
* vote\_for\_witness

### 1.4. CLI Commands for SONs

{% content-ref url="cli-commands-for-sons/" %}
[cli-commands-for-sons](cli-commands-for-sons/)
{% endcontent-ref %}

This reference doc contains the following commands in section 1:

* create\_son
* update\_son
* update\_son_\__vesting\_balance
* get\_son
* vote\_for\_son
* update\_son_\__votes
* list\_sons
* list\_active\_sons
* request\_son\_maintenance
* cancel\_request\_son\_maintenance
* get\_son\_wallets
* get\_active\_son\_wallet
* get\_son\_wallet\_by\_time\_point

This reference doc contains the following commands in section 2:

* add\_sidechain\_address
* delete\_sidechain\_address
* get\_sidechain\_address\_by\_account\_and\_sidechain
* get\_sidechain\_addresses\_by\_account
* get\_sidechain\_addresses\_by\_sidechain
* get\_sidechain\_addresses\_count
