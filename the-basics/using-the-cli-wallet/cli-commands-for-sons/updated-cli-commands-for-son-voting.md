---
description: The cli commands includes voting for various sidechain operators
---

# Updated CLI commands for SON voting

The document explains about SON voting for multiple sidechain listeners like bitcoin, ethereum, & hive and also provide the command which allows the user to change several SON account voting in a single call.

## 1. vote\_for\_son

The latest update on the wallet command allows the user to choose the sidechain operators like Bitcoin, Hive, and Ethereum for voting. It allows you to Vote for any active SONs from this list. Each account's vote is weighted according to the number of PPY owned by that account at the time the votes are tallied. An account can publish a list of all SONs they approve and the cli allows you to add or remove SONs from this list.

The CLI command is given below:

```actionscript
 signed_transaction vote_for_son(string voting_account,
                                 string son,
                                 sidechain_type sidechain,
                                 bool approve,
                                 bool broadcast = false);
```

{% tabs %}
{% tab title="Function call" %}
**Parameters**

<table><thead><tr><th width="170">name</th><th width="157">data type</th><th width="212">description</th><th>details</th></tr></thead><tbody><tr><td>voting_account</td><td>string</td><td>the name or id of the account who is voting with their shares</td><td>No quotes required</td></tr><tr><td>son</td><td>string</td><td>the name or id of the SONs' owner account</td><td>No quotes required</td></tr><tr><td>sidechain</td><td>sidechain_type</td><td>the name of the sidechain - ethereum, hive or bitcoin</td><td>No quotes required</td></tr><tr><td>approve</td><td>bool</td><td><p><strong><code>true</code></strong> if you wish to vote in favor of that SON</p><p><strong><code>false</code></strong> to remove your vote in favor of that SON</p></td><td>n/a</td></tr><tr><td>broadcast = false</td><td>bool</td><td><strong><code>true</code></strong> if you wish to broadcast the transaction</td><td>n/a</td></tr></tbody></table>

**Syntax**

{% code overflow="wrap" %}
```actionscript
Vote_for_son <voting_account_name> <son_account_owner_name> <sidechain_name> <true/false to approve/reject> <true/false(bydefault) to broadcast> 
```
{% endcode %}

**Example Call**

The following examples showcase voting/revoking scenario for various sidechains:

1. Account named `account01` vote for account named `sonaccount01` to become an active son for bitcoin sidechain.

```actionscript
vote_for_son account01 sonaccount01 bitcoin true true
```



2. Account named `account01` revokes its vote for account named `sonaccount01` from the bitcoin sidechain.

```actionscript
vote_for_son account01 sonaccount01 bitcoin false true
```



3. Account named `sonaccount01` vote for account named `sonaccount01` to become an active SON for ethereum sidechain.

```actionscript
vote_for_son sonaccount01 sonaccount01 ethereum true true
```



4. Account named `sonaccount01` revokes its vote for account named `sonaccount01` from ethereum sidechain.

{% code fullWidth="false" %}
```actionscript
vote_for_son sonaccount01 sonaccount01 ethereum false true
```
{% endcode %}
{% endtab %}

{% tab title="Return" %}
The signed transaction changing your vote for the given SON


{% endtab %}
{% endtabs %}

## 2. update\_son\_votes

This command allows you to add or remove **one or more SON** from this list in one call. An account can publish a list of all SONs they approve. Each account's vote is weighted according to the number the core asset shares owned by that account at the time the votes are tallied. When you are changing your vote on several SONs, this may be easier than multiple `vote_for_sons` and `set_desired_witness` & `committee_member_count` calls.

The cli command is given below:

<pre class="language-actionscript" data-overflow="wrap"><code class="lang-actionscript">signed_transaction update_son_votes(string voting_account,
<strong>                                    std::vector&#x3C;std::string> sons_to_approve,
</strong>                                    std::vector&#x3C;std::string> sons_to_reject,
                                    sidechain_type sidechain,
                                    uint16_t desired_number_of_sons,
                                    bool broadcast = false);

</code></pre>

{% tabs %}
{% tab title="Function call" %}
<table><thead><tr><th width="170">name</th><th width="157">data type</th><th width="212">description</th><th>details</th></tr></thead><tbody><tr><td>voting_account</td><td>string</td><td>the name or id of the account who is voting with their shares</td><td>No quotes required</td></tr><tr><td>son_to_approve</td><td>std::vector std::string</td><td>the names or ids of the sons owner accounts you wish to approve (these will be added to the list of sons you currently approve). This list can be empty.</td><td>Quotes required</td></tr><tr><td>sons_to_reject</td><td>std::vector std::string</td><td>the names or ids of the SONs owner accounts you wish to reject (these will be removed from the list of SONs you have approved). This list can be empty.</td><td>Quotes required</td></tr><tr><td>sidechain</td><td>sidechain_type</td><td>the name of the sidechain - ethereum, hive or bitcoin</td><td>No quotes required</td></tr><tr><td>desired_number_of_sons</td><td>unit16_t</td><td>the parameter helps the user to vote for /suggest the number of active SONs in sidechain</td><td>Mention the value in numbers</td></tr><tr><td>broadcast = false</td><td>bool</td><td><strong><code>true</code></strong> if you wish to broadcast the transaction</td><td>n/a</td></tr></tbody></table>

**Syntax**

{% code overflow="wrap" %}
```actionscript
update_son_votes <voting_account_name> [list_of_sonaccount_names_to_approve] [list_of_sonaccount_names_to_reject] <sidechain_name> <number_of_sonacounts_added/rejected> <true/flase(bydefault) to broadcast>
```
{% endcode %}

**Example call**

Account named `account05` votes for accounts named `sonaccount01`, `sonaccount02`, `sonaccount03`, `sonaccount04`, `sonaccount05` and revokes its votes for accounts named `sonaccount06`,`sonaccount07`to become an active SONs. The total number of votes for active SONs is 5 for hive network.

{% code overflow="wrap" %}
```actionscript
update_son_votes account05 [sonaccount01, sonaccount02, sonaccount03, sonaccount04, sonaccount05] [sonaccount06, sonaccount07] hive 5 true
```
{% endcode %}

Account named `eth-6-son` votes for accounts named `eth-2-son`, `eth-3-son`, `eth-5-son`, `eth-6-son`  to become active SON on ethereum and no account is revoked from the sidechain.  The total number of votes for active SONs is 5 for ethereum network.

{% code overflow="wrap" %}
```actionscript
update_son_votes eth-6-son ["eth-2-son", "eth-3-son", "eth-4-son", "eth-5-son", "eth-6-son"] [] ethereum 5 true
```
{% endcode %}
{% endtab %}

{% tab title="Return" %}
The signed transaction changing your vote for the given witnesses
{% endtab %}
{% endtabs %}

{% hint style="warning" %}
Note that you **cannot vote against a SON**, you can either vote for the SON or ignore voting for the SON.&#x20;
{% endhint %}
