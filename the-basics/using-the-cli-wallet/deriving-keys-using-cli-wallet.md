# Deriving Keys using CLI Wallet

There are three types of keys on chain and it can be generated using the CLI wallet. The types of keys are:

1. Active
2. Owner
3. Memo

Follow the below steps to generate keys in the wallet

### 1. Set a password and unlock the wallet

In a **new** command line window, we can access the cli\_wallet program after all the blocks have been downloaded from the chain. Note that "your-password-here" is a password that you're creating for the cli\_wallet and doesn't necessarily have to be the password you used while creating Peerplays account.

```
cli_wallet
set_password your-password-here
unlock your-password-here
```

The CLI wallet will show `unlocked >>>` when successfully unlocked.

A list of CLI wallet commands is available here: [https://devs.peerplays.tech/api-reference/wallet-api/wallet-calls](https://devs.peerplays.tech/api-reference/wallet-api/wallet-calls)

### 2. Generate OWNER private keys for the cli\_wallet and import them

The below command will return an array with your owner key in the form of `["PPYxxx", "xxxx"] ["TESTxxx", "xxx"].`

Note that the "created-username" and "created-password" used here are the username and password from the Peerplays-DEX account created.

```
get_private_key_from_password created-username owner created-password
```

The second value in the returned array is the private key of your owner key. Now we'll import that into the cli\_wallet.

```
import_key "created-username" SECONDVALUEFROMLASTCOMMAND
```

<figure><img src="../../.gitbook/assets/import-owner-key.JPG" alt=""><figcaption></figcaption></figure>

### 3. Generate ACTIVE private keys for the cli\_wallet and import them

The below command will return an array with your active key in the form of `["PPYxxx", "xxxx"] / ["TESTxxx", "xxx"].`

Note that the "created-username" and "created-password" used here are the username and password from the Peerplays-DEX account

```
get_private_key_from_password created-username active created-password
```

The second value in the returned array is the private key of your active key. Now we'll import that into the cli\_wallet.

```
import_key "created-username" SECONDVALUEFROMLASTCOMMAND
```

The keys that begin with "PPY"/"TEST" are the public keys.&#x20;

<figure><img src="../../.gitbook/assets/import-active-key.JPG" alt=""><figcaption></figcaption></figure>
