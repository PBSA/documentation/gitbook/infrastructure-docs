# How to create a Peerplays Account?

We'll need an account as the basis of creating a new Witness. If you don't have an existing account, the easiest way to create one is to use the [Peerplays DEX](https://swap.peerplays.com).

## 1. Steps to register a new Peerplays account

The first peerplays account can be created in a flash using Peerplays DEX by following the steps in below document,

{% embed url="https://community.peerplays.com/technology/peerplays-nex/user-guide/account-creation" %}

Peerplays provides two networks on which  the user may create an account - main net and test net. To learn and familiarize the operations of a node use the test net account. After learning the process and to work on the real Peerplays network, use the main-net account. The [User Guide](https://community.peerplays.com/technology/peerplays-dex-1/user-guide) will help you navigate Peerplays DEX and learn about its features and options in detail.

Click the URL below to create an account and then to login Peerplays DEX,

1. **Mainnet Peerplays DEX access**

Click the below link to use the Main-net DEX:

{% embed url="https://swap.peerplays.com" %}

2. **Testnet Peerplays DEX access**

Click the below link to use the Test-net DEX:

{% embed url="https://testnet.peerplays.download/" %}

## 2. Account creation with Peerplays DEX

Choose the mainnet/testnet login link based on the requirement

1. Choose an account name to register. In case, if the name already exists it will be denoted and please choose any other name.

{% hint style="info" %}
There is an elevated fee for “premium” account names that Peerplays DEX won’t cover, so be sure to pick a name with at least one numeral, hyphen, or dot.
{% endhint %}

2. Peerplays DEX will generate a highly random password for your account and there is no option to choose your desired password.

{% hint style="danger" %}
Please save the auto-generated password at some location. As the account cannot be recovered without the password.
{% endhint %}

In the backend, the private key will be generated using the account name, account password along with few other details. This private key plays a vital role in controlling the account.

## 3. Importing account into cli\_wallet

The account can be imported into cli\_wallet by using the primary key. There are two ways to determine the private key:

### A. Determine Private keys

#### 1) Using NEX UI

1. Login to NEX with account name and password. Click on the S**ettings** option from the right pane and select the **Key Management** tab. &#x20;
2. Now, enter the password and select the required key to be generated i.e., Owner, Active, or/and Memo.
3. Next, click on the **Let's go** button to generate the keys. Click the reveal icon to view the Keys and there is an option to copy the key.

<figure><img src="../.gitbook/assets/Getting-private-key.JPG" alt=""><figcaption></figcaption></figure>

4. To download the keys, Click on **Download Generated Keys** option and save the keys for future reference.

#### 2) Using cli\_wallet

1. The key can be generated using the account name and password in the cli\_wallet

**Syntax :** `get_private_key_from_password account_name role password`

account\_name - Name of the account

Role - Active/Owner/Memo

password - account's password

**Example:**

{% code overflow="wrap" %}
```shell
get_private_key_from_password alice active gXSP03IJ9ZmcfJJxrwl8LO44OXVJxSct3oGmV0ZlyVT55pbmlPvL
```
{% endcode %}

### B. Import key into cli\_wallet

1. After getting the Active key that controls the account, it can be imported into cli\_wallet using the command below:

**Syntax:** `import_key account_name_or_id wif_key`

account\_name\_or\_id - name of the account/id

wif\_key - active, owner, or memo keys

**Example:**

{% code overflow="wrap" %}
```sh
import_key alice 5K8Uwc2mKrHbTQRFjaS3u8UMteLQYooGujsRW1mCLjkg5WNLTJG
```
{% endcode %}

{% hint style="info" %}
1. Use **`gethelp set_password`**`and`**`gethelp unlock`** commands to know in detail about setting password and unlocking wallet.
2. cli\_wallet “wallet password” is NOT the same thing as an account password. The cli\_wallet command maintains a wallet file that stores as many private keys for as many accounts as you wish to add to it. The wallet password is an encryption password used to protect this file. On the other hand, an account password is a password used in the derivation of the various private keys that control a particular account.
{% endhint %}
