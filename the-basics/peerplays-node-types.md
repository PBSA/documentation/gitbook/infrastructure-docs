---
description: An explanation of the many types of Peerplays nodes.
---

# Peerplays Node Types

## 1. Node Types

All Peerplays nodes continually update an internal database representing "consensus state" by applying transactions that arrive in incoming blocks received from the network.  Peerplays nodes communicate with each other using a decentralized Peer-to-Peer (P2P) networking protocol in order to share these blocks and transactions.  Peerplays nodes are sometimes called "witness nodes" because they observe, or "witness", blocks and transactions from the network, and then validate, apply, and optionally share them with other nodes on the network.  The difference between the node types lies in number of services they are configured to offer to the network.  These differences can affect how resource intensive the running of the node is, and may affect what networking ports need to be exposed, and what additional infrastructure (like DNS records) are needed or recommended to support the node.  Some roles and configuration categories are described below:

### 1.1. Block-producing witness nodes

As the name suggests, these nodes produce blocks for the network.  They are run by elected "witnesses".  A witness is a special account on the chain that has declared an intent to produce blocks, and has been elected via an on-chain voting process.  Each witness node validates all blocks and transactions that it receives. The elected block producing nodes all take turns in bundling new transactions into blocks and broadcasting them to the network.

Block-producing witness nodes are often minimally-configured and do not offer additional services to the network (such as client-facing APIs).  They often will not have DNS records nor will their locations or IP addresses be made publicly known.  This helps protect the integrity of block production on the Peerplays network.&#x20;

### 1.2. API nodes

API nodes provide network services to client applications. They allow these client apps to inspect the state of the network, to broadcast new transactions, and other services.  They often retain detailed account and market histories accessible through API calls, and other useful data for client apps that go beyond simple consensus state.  However, they can vary in the amount of available history or extended data. In addition to participating in the Peer-to-Peer network for sharing and receiving blocks, these nodes listen on a designated port to expose the API that client applications use.

API nodes may be public-facing, or they may be deployed for personal or private business use.  If public-facing, they will often be assigned a DNS record, and may additionally be configured behind a reverse proxy to enforce TLS-encrypted connections between client apps and the node.  If the node is for private use, these extras may not be needed.

#### 1.2.1. Full nodes

This is an API node that is maximally configured with a complete transaction history of all accounts.

### 1.3. Seed nodes

Seed nodes accept incoming P2P connections from the network and relay blocks and transactions. They are usually the first nodes contacted by a freshly started node, and help those nodes get up to date and discover the rest of the network. They are the entry point into the network. Once a node has entered the network it will receive additional node addresses from its peers, so all nodes can connect to each other.  A seed node runs the bare minimum services needed to participate in the P2P network, but it may also run additional services if so configured. Thus, a seed node may also be an API node. Seed nodes assist the network by recording and sharing blocks and by being a point of contact for other nodes on the P2P network.

### 1.4. BOS nodes

_Bookie Oracle System nodes_ - BOS nodes are required to operate the Bookie Oracle System to ensure the accuracy and decentralization of the data fed into the BookiePro application. The BOS node must be run on a separate server to the Witness node.

### 1.5. SONs

_Sidechain Operator Nodes_ - SONs facilitate the transfer of off-chain assets (like Bitcoin, Hive, or Ethereum tokens) between the Peerplays chain and the asset's native chain. These nodes often run the Peerplays node software and node software of other chains.

{% hint style="info" %}
The software used to run Witness, API (full), Seed, and SON nodes is named `witness_node`. All these node types are run with the same software. What makes these nodes different is how that software is configured and how it's used.

SONs will also require the use of software supplied by other chains, like Bitcoin Core for example.

BOS nodes use a collection of software known as the Bookie Oracle Suite.
{% endhint %}

## 2. Summary

|      Node Type | Description                                                                                                                             |     Open Ports    |
| -------------: | --------------------------------------------------------------------------------------------------------------------------------------- | :---------------: |
| Block Producer | Elected by the community to produce blocks of validated transactions.                                                                   |        None       |
|     API (Full) | Provides an API gateway for apps to interact with the Peerplays chain. Full nodes offer the whole transaction history for all accounts. |        RPC        |
|           Seed | Opening a P2P port allows new nodes to more readily perform the initial download of the Peerplays chain.                                |        P2P        |
|            BOS | BOS nodes are whitelisted by Witnesses to feed data to the BookiePro app.                                                               |        SSL        |
|            SON | Elected by the community to facilitate asset transfers between the Peerplays chain and sidechains.                                      | Likely (see note) |

{% hint style="warning" %}
SONs most likely will be running other nodes (like a Bitcoin node) which may require opening ports to operate on the sidechain. It is because of this that SON nodes should not be run in parallel (i.e. the same server) with Witness nodes.
{% endhint %}



## 3. References

* Node server [hardware requirements](hardware-requirements.md).
* Details about [Witness Nodes](../witnesses/installation-guides/).
* Details about [SON Nodes](../sidechain-operator-nodes-sons/installation-guides/).
