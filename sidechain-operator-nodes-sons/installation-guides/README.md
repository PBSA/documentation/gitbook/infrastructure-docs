# Installation Guides

## [Requirements to Run a SON Node](../../the-basics/hardware-requirements.md)

The minimum system requirements of the server which will host your SON Node.

## [SON Configuration](son-configuration-version-1.5.19.md)

Example config.ini settings and their explanations for SON Node operation.

## [Install SON Manually](manual-install.md)&#x20;

Compile and run the source code without a Docker container.

## [Install SON with Docker](docker-install.md)

Use a pre-configured Docker container to run a SON Node.

## [<mark style="color:blue;">Bitcoin-SONs Sanity Checks</mark>](bitcoin-sons-sanity-checks.md)

CLI checks to ensure the successful installation of Bitcoin-SON node.

##
