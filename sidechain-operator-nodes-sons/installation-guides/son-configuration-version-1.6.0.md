# SON configuration - Version 1.6.0

## Configuration file

The `config.ini` file consist of all the necessary details to configure the node based on the operator's requirements. It has the option to enable/disable the use of specific asset, endpoint, wallets, etc.,&#x20;

The set of active plugin options such as witness, debug\_witness, account\_history, market\_history, and peerplay\_sidechain are available.

There are other set of plugins such as elastic\_search, es\_object, snapshot, and delayed\_node available to use based on the operator's requirements. (Disabled by default)

The config.ini can be divided into two section,

1. End point configuration
2. Plugin options (only active configurations are explained here)

## 1. End point configuration

The user must provide the details such as which network interfaces & ports to listen on and which seed nodes to use for peer discovery.  Also, to be a SONs operator, the relevant plugin must be enabled.

The default configuration file looks as follows:

{% code title="Default End-point config " overflow="wrap" %}
```ini
# Endpoint for P2P node to listen on
p2p-endpoint = 0.0.0.0:9777

# P2P nodes to connect to on startup (may specify multiple times)
# seed-node = 

# JSON array of P2P nodes to connect to on startup
seed-nodes = []

# Pairs of [BLOCK_NUM,BLOCK_ID] that should be enforced as checkpoints.
# checkpoint = 

# Endpoint for websocket RPC to listen on
rpc-endpoint = 127.0.0.1:8090

# Endpoint for TLS websocket RPC to listen on
# rpc-tls-endpoint = 

# The TLS certificate file for this server
# server-pem = 

# Password for this certificate
# server-pem-password = 

# File to read Genesis State from
genesis-json = /peerplays/son-genesis.json

# Block signing key to use for init witnesses, overrides genesis file
# dbg-init-key = 

# JSON file specifying API permissions
# api-access = 

# Whether to enable tracking of votes of standby witnesses and committee members. Set it to true to provide accurate data to API clients, set to false for slightly better performance.
# enable-standby-votes-tracking = 

# Space-separated list of plugins to activate
plugins = account_history accounts_list affiliate_stats bookie market_history witness
```
{% endcode %}

Only the essential details to configure SON node is mentioned below. The operator must provide their details for configuration.  Example configuration values as follows:

{% code title="Example SON configuration" overflow="wrap" %}
```ini
# Endpoint for P2P node to listen on
p2p-endpoint = 0.0.0.0:9777

# P2P nodes to connect to on startup (may specify multiple times)
#Mention your node detail
seed-node = 65.21.236.76:9777

# JSON array of P2P nodes to connect to on startup
seed-nodes = []

# Endpoint for websocket RPC to listen on
rpc-endpoint = 0.0.0.0:8090

# Space-separated list of plugins to activate
#Include peerplays_sidechain plugin to use 
plugins = account_history accounts_list affiliate_stats bookie market_history witness peerplays_sidechain
```
{% endcode %}

## 2. Plugin Options

The list of active plugins available in the config file are,

* witness
* debug\_witness
* account\_history
* market\_history
* peerplay\_sidechain

### 2.1 witness

The witness plugin default config is provided below and there is no manual configuration required if the SON operator is not also a block-producing witness on the same machine.

{% code overflow="wrap" %}
```ini
# ==============================================================================
# witness plugin options
# ==============================================================================

# Enable block production, even if the chain is stale.
enable-stale-production = false

# Percent of witnesses (0-99) that must be participating in order to produce blocks
required-participation = false

# ID of witness controlled by this node (e.g. "1.6.5", quotes are required, may specify multiple times)
#Example
witness-id = "1.6.1"
witness-id = [1.6.2]

# IDs of multiple witnesses controlled by this node (e.g. ["1.6.5", "1.6.6"], quotes are required)
# List of witness IDs belong to your node
#Example
witness-ids = ["1.8.2","1.8.3"]

# Tuple of [PublicKey, WIF private key] (may specify multiple times)
private-key = ["TEST6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV","5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXt5g6zdfvFD3"]

```
{% endcode %}

### 2.2 debug\_witness

The debug\_witness default config is provided below. By default the keys will be added after the node creation,

{% code overflow="wrap" %}
```ini
# ==============================================================================
# debug_witness plugin options
# ==============================================================================

# Tuple of [PublicKey, WIF private key] (may specify multiple times)
debug-private-key = ["TEST6MRyAjQq8ud7hVNYcfnVPJq..............","5KQwrPbwdL6P.....yXtP79zkvFD3"]
```
{% endcode %}

### 2.3 account\_history

The account\_history default config is provided below and no manual changes are required unless your use case necessitates it.

{% code overflow="wrap" %}
```ini
# ==============================================================================
# account_history plugin options
# ==============================================================================

# Account ID to track history for (may specify multiple times)
# track-account = 

# Keep only those operations in memory that are related to account history tracking
partial-operations = 1

# Maximum number of operations per account will be kept in memory
max-ops-per-account = 100
```
{% endcode %}

### 2.4 market\_history

The market\_history default config is provided below and no manual changes are required unless required.

{% code overflow="wrap" %}
```ini
# ==============================================================================
# market_history plugin options
# ==============================================================================

# Track market history by grouping orders into buckets of equal size measured in seconds specified as a JSON array of numbers
bucket-size = [15,60,300,3600,86400]

# How far back in time to track history for each bucket size, measured in the number of buckets (default: 1000)
history-per-size = 1000
```
{% endcode %}

### 2.5 peerplays\_sidechain

This plugin consists of all the necessary details about various asset, IPs, private keys, wallet details, and API endpoints. In order to make the SON node work as required, the operator must carefully input their values and requirement in this configuration.

The default plugin configuration is mentioned below,

<details>

<summary>Default configuration</summary>

{% code overflow="wrap" %}
```ini
# ==============================================================================
# peerplays_sidechain plugin options
# ==============================================================================

# ID of SON controlled by this node (e.g. "1.33.5", quotes are required)
# son-id = 

# IDs of multiple SONs controlled by this node (e.g. ["1.33.5", "1.33.6"], quotes are required)
# son-ids = 

# Tuple of [PublicKey, WIF private key] (may specify multiple times)
peerplays-private-key = ["TEST6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV","5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3"]

# Sidechain retry throttling threshold
sidechain-retry-threshold = 150

# Outputs RPC calls to console
debug-rpc-calls = 0

# Simulate RPC connection reselection by altering their response times by a random value
simulate-rpc-connection-reselection = 0

# Bitcoin sidechain handler enabled
bitcoin-sidechain-enabled = 0

# IP address of Bitcoin node
bitcoin-node-ip = "127.0.0.1"

# Use bitcoind client instead of libbitcoin client
use-bitcoind-client = 0

# Libbitcoin server IP address
libbitcoin-server-ip = 127.0.0.1

# Block ZMQ port of libbitcoin server
libbitcoin-server-block-zmq-port = 9093

# Trx ZMQ port of libbitcoin server
libbitcoin-server-trx-zmq-port = 9094

# ZMQ port of Bitcoin node
bitcoin-node-zmq-port = 11111

# RPC port of Bitcoin node
bitcoin-node-rpc-port = 8332

# Bitcoin RPC user
bitcoin-node-rpc-user = 1

# Bitcoin RPC password
bitcoin-node-rpc-password = 1

# Bitcoin wallet name
# bitcoin-wallet-name = 

# Bitcoin wallet password
# bitcoin-wallet-password = 

# Tuple of [Bitcoin public key, Bitcoin private key] (may specify multiple times)
bitcoin-private-key = ["02d0f137e717fb3aab7aff99904001d49a0a636c5e1342f8927a4ba2eaee8e9772","cVN31uC9sTEr392DLVUEjrtMgLA8Yb3fpYmTRj7bomTm6nn2ANPr"]

# Ethereum sidechain handler enabled
ethereum-sidechain-enabled = 0

# Ethereum node RPC URL [http[s]://]host[:port]
ethereum-node-rpc-url = "127.0.0.1:8545"

# Ethereum RPC user
# ethereum-node-rpc-user = 

# Ethereum RPC password
# ethereum-node-rpc-password = 

# Ethereum wallet contract address
# ethereum-wallet-contract-address = 

# Tuple of [ERC-20 symbol, ERC-20 address] (may specify multiple times)
# ethereum-erc-20-address = 

# Tuple of [Ethereum public key, Ethereum private key] (may specify multiple times)
ethereum-private-key = ["5fbbb31be52608d2f52247e8400b7fcaa9e0bc12","9bedac2bd8fe2a6f6528e066c67fc8ac0622e96828d40c0e820d83c5bd2b0589"]

# Hive sidechain handler enabled
hive-sidechain-enabled = 0

# Hive node RPC URL [http[s]://]host[:port]
hive-node-rpc-url = "127.0.0.1:28090"

# Hive node RPC user
# hive-node-rpc-user = 

# Hive node RPC password
# hive-node-rpc-password = 

# Hive wallet account name
# hive-wallet-account-name = 

# Tuple of [Hive public key, Hive private key] (may specify multiple times)
hive-private-key = ["TST6LLegbAgLAy28EHrffBVuANFWcFgmqRMW13wBmTExqFE9SCkg4","5JNHfZYKGaomSFvd4NUdQ9qMcEAC43kujbfjueTHpVapX1Kzq2n"]
```
{% endcode %}

</details>

An example configuration to enable basic requirements for a SON node is explained below,

{% code title="Example plugin configuration" overflow="wrap" %}
```ini
# ==============================================================================
# peerplays_sidechain plugin options
# ==============================================================================

# ID of SON controlled by this node (e.g. "1.33.5", quotes are required)
#example
son-id ="1.22.7"
son-id=[1.22.9] 

# IDs of multiple SONs controlled by this node (e.g. ["1.33.5", "1.33.6"], quotes are required)
#Mention the relevant SON-ID
son-ids = ["1.33.7","1.33.8"]

# Tuple of [PublicKey, WIF private key] (may specify multiple times)
peerplays-private-key = ["TST6LLegbAgLAy28EHrffBVuANFWcFgd8...","5JNHfZYKomSFvd4NUdQ9qMcEAC43ku..."]

# Sidechain retry throttling threshold
sidechain-retry-threshold = 150

# Outputs RPC calls to console
debug-rpc-calls = 0

# Simulate RPC connection reselection by altering their response times by a random value
simulate-rpc-connection-reselection = 0

# Bitcoin sidechain handler enabled
#Replace the value from 0 to 1
bitcoin-sidechain-enabled = 1

# IP address of Bitcoin node
bitcoin-node-ip = "127.0.0.1"

# Use bitcoind client instead of libbitcoin client
#Replace the value from 0 to 1 
use-bitcoind-client = 1

#if bitcoind is set to 1, by default the libbitcoin operation will be ignored, 
# Libbitcoin server IP address
libbitcoin-server-ip = 127.0.0.1

# Block ZMQ port of libbitcoin server
libbitcoin-server-block-zmq-port = 9093

# Trx ZMQ port of libbitcoin server
libbitcoin-server-trx-zmq-port = 9094

# ZMQ port of Bitcoin node
bitcoin-node-zmq-port = 11111

# RPC port of Bitcoin node
bitcoin-node-rpc-port = 8332

# Bitcoin RPC user
#Mention the user name
bitcoin-node-rpc-user = user1

# Bitcoin RPC password
#Mention the respective password
bitcoin-node-rpc-password = password

# Bitcoin wallet name
#Mention the wallet name
bitcoin-wallet-name = son-account

# Bitcoin wallet password
#Mention the wallet password
bitcoin-wallet-password = password

# Tuple of [Bitcoin public key, Bitcoin private key] (may specify multiple times)
#
bitcoin-private-key = ["02d0f137e717fb3abdc7aff99904001d...................","C4r392DLVUEjrtMgLA8Yb3..."]

# Ethereum sidechain handler enabled
#To enable ETH-SON, replace 0 by 1
ethereum-sidechain-enabled = 1

# Ethereum node RPC URL [http[s]://]host[:port]
ethereum-node-rpc-url = "127.0.0.1:8545"

#if ETH is enable provide the necessary details below
# Ethereum RPC user
# ethereum-node-rpc-user = 

# Ethereum RPC password
# ethereum-node-rpc-password = 

# Ethereum wallet contract address
# ethereum-wallet-contract-address = 

# Tuple of [ERC-20 symbol, ERC-20 address] (may specify multiple times)
# ethereum-erc-20-address = 

# Tuple of [Ethereum public key, Ethereum private key] (may specify multiple times)
ethereum-private-key = ["5fbbb31be5.....................","9bedac2bd8fe2a6f6....."]

# Hive sidechain handler enabled
#To enable HIVE, replace 0 by 1
hive-sidechain-enabled = 1

# Hive node RPC URL [http[s]://]host[:port]
hive-node-rpc-url = "127.0.0.1:28090"

#if HIVE is enable provide the necessary details below
# Hive node RPC user
# hive-node-rpc-user = 

# Hive node RPC password
# hive-node-rpc-password = 

# Hive wallet account name
# hive-wallet-account-name = 

# Tuple of [Hive public key, Hive private key] (may specify multiple times)
hive-private-key = ["TST6LLegbAgLAy28EHrf.....","5JNHfZYKGaomSFvd4NUdQ9......"]

```
{% endcode %}

{% hint style="warning" %}
1. Config file contains all the default public/private keys for SON account, no changes are required here. But unused key pairs may be removed.
2. List of keypairs is ordered. The first one belongs to sonaccount1, last to sonaccount16
{% endhint %}

## How to find the value of a SON account?

By using the cli\_wallet, the SON account information, private key can be collected. Execute the below command in the wallet,

{% code overflow="wrap" %}
```
# login to the cli_wallet
# Getting SON information about sonaccount01

>> get_son sonaccount01

## response will be similar to this
# get_son sonaccount01
# {
#   "id": "1.27.0",
#   "son_account": "1.2.36",
#   "vote_id": "3:38",
#   "total_votes": 0,
#   "url": "http://sonaddreess01.com",
#   "deposit": "1.13.1",
#   "signing_key": "TEST8TCQFzyYDp3DPgWZ24261fMPSCzXxVyoF3miWeTj6JTi2DZdrL",
#   "pay_vb": "1.13.2",
#   "statistics": "2.24.0",
#   "status": "active",
#   "sidechain_public_keys": [[
#       "bitcoin",
#       "03456772301e221026269d3095ab5cb623fc239835b583ae4632f99a15107ef275"
#     ]
#   ]
# }

# Get the private key for signing_key of sonaccount01

unlocked >>> get_private_key TEST8TCQFzyYDp3DPgWZ24261fMPSCzXxVyoF3miWeTj6JTi2DZdrL

# response will be similar to this
# "5K7tjTnFn2zasimVmebUiH1yawgwK9YXm2chUwcurcHgqms9JaF"

# We have all we need
# son-id (1.27.0)
# public key ("TEST8TCQFzyYDp3DPgWZ24261fMPSCzXxVyoF3miWeTj6JTi2DZdrL")
# private keys ("5K7tjTnFn2zasimVmebUiH1yawgwK9YXm2chUwcurcHgqms9JaF")
```
{% endcode %}

The config parameter should look like the below example,

{% code overflow="wrap" %}
```ini
son-id = "1.27.0"
peerplays-private-key = ["TEST8TCQFzyYDp3DPgWZ24261fMPSCzXxVyoF3miWeTj6JTi2DZdrL","5K7tjTnFn2zasimVmebUiH1yawgwK9YXm2chUwcurcHgqms9JaF"]
```
{% endcode %}
