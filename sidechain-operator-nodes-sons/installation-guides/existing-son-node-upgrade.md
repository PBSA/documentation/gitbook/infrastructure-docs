# Existing SON node upgrade

## Introduction

Peerplays Core release 1.6.0 will introduce changes to SONs operations, and SONs operators (those acting as a SON on the network) will need to upgrade to the latest software _and_ make changes to their node configuration in the `config.ini` file.  These changes are critical to ensure un-interupted SONs network services.

## Overview of SONS-related changes in 1.6.0

A brief overview of changes is given here, followed by details in subsequent sections, including changes SONs operators will need to make to their nodes.

#### General:

* From 1.6.0 forward, following the hard-fork date, it will be possible for SONs operators to operate a subset of SONs.  They will no longer be required to operate _all_ supported SONs.  I.e., if they wish to operate a Bitcoin SON but _not_ a Hive SON, this will be possible following the hard fork.

#### For Bitcoin SONs:

* 1.6.0 Introduces experimental support for Libbitcoin.&#x20;
  * _**Note:** As support is experimental, it is recommended to continue using a **bitcoind** API access point at this time._&#x20;
* 1.6.0 renames a configuration parameter for clarity.  Existing `config.ini` files will need to be adjusted.
* 1.6.0 adds some new configuration parameter, of which a few are mandatory.  Existing `config.ini` files will need to be adjusted.

#### For Hive SONs:

* 1.6.0 adds a mandatory configuration parameter.  Existing `config.ini` files will need to be adjusted.&#x20;

#### New: Ethereum SONs:

* 1.6.0 adds support for Ethereum SONs.  SONs operators MAY add an Ethereum SON to their operations.&#x20;

## 1. Bitcoin SON Operators

With this new upgrade, the bitcoin operators have two option to use for interfacing with the Bitcoin network. The two Bitcoin API endpoint options are,

1. Bitcoind
2. Libbitcoin (Experimental)  (**Note:** The current recommendation is to continue using bitcoind until support for Libbitcoin reaches maturity.)

{% hint style="danger" %}
_**Important:** Main-net SONs operators are **recommended to use the bitcoind option** at this point as the libbitcoin feature is considered experimental in this release.  Test-net SONs operators may try out Libbitcoin if desired._
{% endhint %}

### New Parameters&#x20;

Operators upgrading from a previous version will need to edit their existing `config.ini` file, as new config options are available and some are required in this release.

1. **Parameter: `use-bitcoind-client` (Recommended to use)**\
   This option is used to select **bitcoind** as the API protocol for accessing the Bitcoin network. The user must add this option to their config and change the value from 0 to 1 to enable its usage. (Otherwise it will default to libbitcoin.)

{% code overflow="wrap" %}
```ini
# ==============================================================================
# peerplays_sidechain plugin options
# ==============================================================================

# ...

#Use bitcoind client instead of libbitcoin client
use-bitcoind-client = 1
```
{% endcode %}

2. **Libbitcoin parameters**\
   The use of libbitcoin is not yet recommended for Main-net.  These options MAY be added to the config file, but will be ignored so long as bitcoind is enabled.  Example values are as follows:

<pre class="language-ini"><code class="lang-ini"># ==============================================================================
# peerplays_sidechain plugin options
# ==============================================================================

# ...

<strong># Libbitcoin server IP address
</strong>libbitcoin-server-ip = 127.0.0.1

# Block ZMQ port of libbitcoin server
libbitcoin-server-block-zmq-port = 9093

# Trx ZMQ port of libbitcoin server
libbitcoin-server-trx-zmq-port = 9094

</code></pre>

### Renamed Parameters

1. **Parameter: `bitcoin-wallet-name`**

In 1.6.0, the **`bitcoin-wallet`** option in config.ini is renamed to **`bitcoin-wallet-name`** . The old option will not be recognized.&#x20;

```ini
# ==============================================================================
# peerplays_sidechain plugin options
# ==============================================================================

#Bitcoin wallet name
bitcoin-wallet-name = mybitcoinwallet

```

{% hint style="warning" %}
If the input is not updated, the SONs will not function. The following warning message will be shown at `witness_node` startup:,

**" Haven’t set up Bitcoin sidechain parameters "**
{% endhint %}

## 2. HIVE SON Operators

After the upgrade, the HIVE plugin has only one new parameter added.

## New parameter

1. **hive-wallet-account-name**\
   The input for this parameter is the SON multisig account name on the HIVE network which is controlled by the SONs operators. This is the `son-account` on HIVE mainnet.

```ini
# ==============================================================================
# peerplays_sidechain plugin options
# ==============================================================================

# ...

# Hive wallet account name
hive-wallet-account-name = son-account

```

## 3. ETH SON Operators

New in this release, a SON operator may operate an ETH SON.  Operators who wish to enable an ETH-SON can follow the steps from the link below,



{% content-ref url="eth-sons-configuration-and-installation.md" %}
[eth-sons-configuration-and-installation.md](eth-sons-configuration-and-installation.md)
{% endcontent-ref %}
