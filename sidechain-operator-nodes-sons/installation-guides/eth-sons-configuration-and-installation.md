# ETH-SONs Configuration & Installation

SONs manual installation process is similar to witness node installation. However, the SONs will perform the assets transfer between Peerplays blockchain and other blockchains, we need to connect to another chain to be of any use. This document features the steps to configure and install Ethereum-SON.&#x20;

Let's use Ethereum! 😃 :coin:&#x20;

The following steps outline the Manual Installation of a ETH-SON.

1. Build Peerplays node
2. Primary Requirements
3. Installation steps
4. Peerplays ETH-SON configuration
5. Start the SON

{% hint style="info" %}
Before we begin, to set up a SON node requires about 110 PPY. This is to pay for an upgraded account (5 PPY) and to fund two vesting balances (50 PPY each). The remaining funds are to pay for various transaction fees while setting up the node. Please see [Obtaining Your First Tokens](../../the-basics/obtaining-your-first-tokens.md) for more info.

Note that these fees will likely change over time as recommended by the Committee of Advisors.
{% endhint %}

## 1. Build Peerplays node&#x20;

The detailed steps to build the Peerplays node is explained in the below readme file. Click the link to follow the steps.

{% embed url="https://gitlab.com/PBSA/peerplays/-/blob/master/README.md?ref_type=heads" %}
Build and Install steps
{% endembed %}

It covers the initial steps in bringing up the node which includes latest Ubuntu installation and its software dependencies, building Peerplays, building docker images, starting & upgrading a Peerplays node, wallet setup, and finally witness node creation.

## 2. ETH-SON Operator's Primary requirements

* A Peerplays account to act as the SON operator.
* An Ethereum account to join the ETH SON multisig on the ETH chain. The account can be created using Metamask, wallet, etc.,
* An accessible Ethereum API node to communicate with the Ethereum network.

## 3. Installation steps

The SON node installation steps are explained in the below section. Click the link based on your preferences,

{% embed url="https://infra.peerplays.com/sidechain-operator-nodes-sons/installation-guides/manual-install#4.-use-the-cli-wallet-to-create-a-peerplays-son-account" %}

{% embed url="https://infra.peerplays.com/sidechain-operator-nodes-sons/installation-guides/docker-install#4.-installing-the-peerplays-son-image" %}

## 4. Peerplays ETH-SON configuration

The generated `config.ini` file will be located at `/home/ubuntu/witness_node_data_dir/config.ini`. We'll begin by editing this config file.

```ini
cd /home/ubuntu/witness_node_data_dir
vim config.ini
```

The config file is large and only the required section is focused here. This section contains all the SON related configuration. Ensure the following config settings are in the `config.ini` file under the peerplays\_sidechain plugin options.

{% code title="Example configuration" overflow="wrap" %}
```ini
# ==============================================================================
# peerplays_sidechain plugin options
# ==============================================================================

# ID of SON controlled by this node (e.g. "1.27.5", quotes are required)
son-id = "1.27.16"

# IDs of multiple SONs controlled by this node (e.g. ["1.27.5", "1.27.6"], quotes are required)
# son-ids = 

# Tuple of [PublicKey, WIF private key] (may specify multiple times)
peerplays-private-key = ["PPY7SUmjftH3jL5L1YCTdMo1hk5qpZrhbo4MW6N2wWyQpjXkT7ByB", "5JKvPJkerMNVEubsbKN8Xd8wGaU1ifhv7xAwy9gFJP6yMEoTkSd"]


# Ethereum sidechain handler enabled
ethereum-sidechain-enabled = 1

# Ethereum node RPC URL [http[s]://]host[:port]
ethereum-node-rpc-url = "10.11.12.202:8545"

# Ethereum RPC user
ethereum-node-rpc-user = username

# Ethereum RPC password
ethereum-node-rpc-password = password

# Ethereum wallet contract address
ethereum-wallet-contract-address = (0x56316f7354845275F28478......fCe6)

# Tuple of [ERC-20 symbol, ERC-20 address] (may specify multiple times)
ethereum-erc-20-address = ["ERC20TEST", "0xC11Ae091d112D6e2E7a45065E307B29e5DF74706"]

# Tuple of [Ethereum public key, Ethereum private key] (may specify multiple times)
ethereum-private-key = ["5fbbb31be52608d2f52247e8400b7fcaa9....","9bedac2bd8fe2a6f6528e066c67fc8ac0622e96828d40c0e820d83c5bd2.."]

```
{% endcode %}

Make sure to add `peerplays_sidechain` plugin along with existing plugins in `config.ini` file. Find the `plugins`  option in the initial section of the `config.ini` file and add the `peerplays_sidechain` plugin to the list as shown below

<pre class="language-ini" data-overflow="wrap"><code class="lang-ini"><strong># Space-separated list of plugins to activate
</strong>
plugins = witness account_history market_history accounts_list affiliate_stats bookie peerplays_sidechain
</code></pre>

{% hint style="danger" %}
In order to enable Ethereum sidechain, replace 0 by 1 in the below command.&#x20;

By default, the value is "0" in the config.ini file.

```ini
ethereum-sidechain-enable=1
```
{% endhint %}

## 5. Start the SON

After setting up the `config.ini` file for SON operation, we'll start the node back up.

```ini
witness_node
```

{% hint style="success" %}
SON node is UP and it's time to play around !
{% endhint %}

## 6. What's Next?

### 6.1. Auto-starting your node

Up until this point we have been running the node in the foreground which is fragile and inconvenient. So let's start the node as a service when the system boots up instead.

{% content-ref url="../../the-basics/auto-starting-a-node.md" %}
[auto-starting-a-node.md](../../the-basics/auto-starting-a-node.md)
{% endcontent-ref %}

### 6.2. Creating a backup node

After that, it would be smart to create a backup server to enable you to make software updates, troubleshoot issues with the node, and otherwise take your node offline without causing service outages.

{% content-ref url="../../the-basics/backup-servers.md" %}
[backup-servers.md](../../the-basics/backup-servers.md)
{% endcontent-ref %}

### 6.3. Fire up another node :fire:&#x20;

Now you have a SON, but have you thought about becoming a Witness? It will be a piece of cake for you since you've already set up a SON.

{% content-ref url="broken-reference" %}
[Broken link](broken-reference)
{% endcontent-ref %}

### 6.4. Enable SSL to encrypt your node traffic

If you have a node that is accessible from the internet (for example, an API or Seed node) it would be wise to enable SSL connections to your node.

{% content-ref url="../../advanced-topics/reverse-proxy-for-enabling-ssl.md" %}
[reverse-proxy-for-enabling-ssl.md](../../advanced-topics/reverse-proxy-for-enabling-ssl.md)
{% endcontent-ref %}

## 7. Glossary

**SON**: Sidechain Operator Node - An independent server operator which facilitates the transfer of off-chain assets (like Bitcoin or Ethereum tokens) between the Peerplays chain and the asset's native chain.

**Witness:** An independent server operator which validates network transactions.

**Witness Node:** Nodes with a closed RPC port. They don't allow external connections. Instead these nodes focus on processing transactions into blocks.

**Ethereum:** Ethereum is a decentralized blockchain with smart contract functionality. Ether is the native cryptocurrency of the platform.
